//+=============================================================================
//
// file :         MaxADCThread.cpp
//
// description :  This class is used for Brilliance MaxADC reading
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaBrilliance_ns
{
class MaxADCThread;
}

#include <MaxADCThread.h>
#include <ITECH/esrf-brilliance-update-2.25/src/cspi-2.25/src/ebpp.h>
#include <ITECH/esrf-brilliance-update-2.25/src/driver-2.25/src/libera.h>
#include "CSPIHelper.h"
#include "MaxADCThread.h"

// Refresh period (in us)
#define MAXADC_LOOP_PERIOD 500000

namespace LiberaBrilliance_ns {


// ----------------------------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------------------------
MaxADCThread::MaxADCThread(LiberaBrilliance *libera, omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), ds(libera)
{

  INFO_STREAM << "MaxADCThread: Starting MaxADCThread Thread." << endl;

  init();
  start_undetached();

}

void MaxADCThread::init() {

  // Initialise connection parameters
  CSPIHelper::ClearConn(conn);
  conn.name = "MaxADCThread";
  memset(&params,0, sizeof(CSPI_ENVPARAMS));
  trigger_cond = PTHREAD_COND_INITIALIZER;
  trigger_mutex = PTHREAD_MUTEX_INITIALIZER;

  threadStatus = "MaxADCThread: connecting";
  exitThread = false;
  isConnected = false;

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *MaxADCThread::run_undetached(void *arg) {

  string errorStr;

  cout << "MaxADCThread: entering." << endl;

  while(!exitThread) {

    // Connection
    while (!isConnected && !exitThread) {

      if (!CSPIHelper::CSPIConnect(conn, errorStr)) {
        // Connect failure
        mutex.lock();
        threadStatus = errorStr;
        mutex.unlock();
        CSPIHelper::CSPIDisconnect(conn);
        sleep(3);
      } else {
        isConnected = true;
      }

    }

    mutex.lock();
    threadStatus = "MaxADCThread: running";
    mutex.unlock();

    while (isConnected && !exitThread) {

      time_t t0 = CSPIHelper::GetTicks();

      // Read environement
      CSPI_BITMASK refreshMask = CSPI_ENV_MAX_ADC;
      int rc = cspi_getenvparam(conn.envHandle, &params, refreshMask);

      time_t t1 = CSPIHelper::GetTicks();

      ds->attr_MaxADC_read[0] = (t1-t0);

      if (rc) {

        // Communication error, go to reconnection
        cerr << "MaxADCThread::cspi_getenvparam() failed " << CSPIHelper::GetCSPIError(rc) << " "
             << CSPIHelper::GetDate() << endl;

        mutex.lock();
        threadStatus = "MaxADCThread: " + CSPIHelper::GetCSPIError(rc);
        mutex.unlock();
        CSPIHelper::CSPIDisconnect(conn);
        isConnected = false;
        push_change_event(-1);

      } else {

        push_change_event(params.max_adc);

      }

      mutex.lock();
      ds->maxAdcBuff.push_back((double) params.max_adc);
      if ((int) ds->maxAdcBuff.size() > ds->attr_AGC_FilterLength_read[0])
        ds->maxAdcBuff.erase(ds->maxAdcBuff.begin());
      ds->maxAdcReadCount++;
      mutex.unlock();

      t1 = CSPIHelper::GetTicks();

      if (isConnected) {

        struct timeval now;
        struct timespec timeout;
        gettimeofday(&now, 0);
        int delayus = (int) (MAXADC_LOOP_PERIOD - (t1 - t0) * 1000);

        // Offset timeout 'delay' seconds in the future.
        time_t nbuSec = now.tv_sec * 1000000 + now.tv_usec + delayus;
        timeout.tv_sec = nbuSec / 1000000;
        timeout.tv_nsec = (nbuSec % 1000000) * 1000;

        pthread_cond_timedwait(&trigger_cond,
                               &trigger_mutex,
                               &timeout);

      }

    }

  }

  mutex.lock();
  threadStatus = "MaxADCThread: off";
  mutex.unlock();

  cout << "MaxADCThread: disconnecting." << endl;

  CSPIHelper::CSPIDisconnect(conn);

  cout << "MaxADCThread: exiting." << endl;

  return NULL;

}

// ----------------------------------------------------------------------------------------
void MaxADCThread::push_change_event(int value) {

  Tango::DevULong *v = new Tango::DevULong;
  *v = value;
  try {
    ds->push_change_event("MaxADC", v, 1, 0, true);
  } catch(Tango::DevFailed &e) {
    cout << "Warning, push_change_event(MaxADC) failed: " << e.errors[0].desc << endl;
  }

}

int MaxADCThread::getMaxADC() {
  if(!isConnected) return 0;
  return params.max_adc;
}

// ----------------------------------------------------------------------------------------

void MaxADCThread::wakeup() {
  // Wake up thread
  pthread_cond_signal( &trigger_cond );
}

} // namespace LiberaBrilliance_ns