//+=============================================================================
//
// file :         CSPIHelper.cpp
//
// description :  CSPI Helper class
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
#include "CSPIHelper.h"

using namespace std;
#include <string.h>
#include <iostream>
#include <sys/time.h>
#include <ITECH/esrf-brilliance-update-2.25/src/cspi-2.25/src/ebpp.h>

#define HANDLE_SERVER_ERROR(code,source)               \
if (code) {                                            \
  error = conn.name + source + " " + GetServerError(); \
  return false;                                        \
}


#define HANDLE_CSPI_ERROR(code,source)                          \
if (code) {                                                     \
  error = conn.name + ": " + source + " " + GetCSPIError(code); \
  return false;                                                 \
}

static std::string ipAddr;
static int port;
static std::string multicastIpAddr;

//#define DPRINT

// -------------------------------------------------------------------------------------------------------------

void CSPIHelper::Init(std::string _ipAddr,int _port,std::string _multicastIpAddr) {

  ipAddr = _ipAddr;
  port = _port;
  multicastIpAddr = _multicastIpAddr;

}

// -------------------------------------------------------------------------------------------------------------

bool CSPIHelper::CSPIConnect(CSPI_CONN &conn,string &error) {

  time_t t0;
  time_t t1;
  int rc;

  // Random sleep [0..10ms] to avoid a burst of DNS request (experimental)
  usleep(rand()%10000);

  // Connect to the Libera generic server
  t0 = CSPIHelper::GetTicks();
  if( multicastIpAddr.length()==0 ) {
    rc = server_connect(ipAddr.c_str(), port, NULL, 0);
  } else {
    rc = server_connect(ipAddr.c_str(), port, multicastIpAddr.c_str(), 0);
  }
  HANDLE_SERVER_ERROR(rc,"CSPIConnect::server_connect");
  t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
  cerr << conn.name << ":server_connect() " << (t1-t0) << "ms " << GetDate() << endl;
#endif

  t0 = CSPIHelper::GetTicks();
  int cache_size = 0;
  int cache_lock = 0;
  rc = server_setparam(protocol::SERVER_CACHE_SIZE, &cache_size);
  HANDLE_SERVER_ERROR(rc, "CSPIConnect::server_setparam");
  rc = server_setparam(protocol::SERVER_CACHE_LOCK, &cache_lock);
  HANDLE_SERVER_ERROR(rc, "CSPIConnect::server_setparam");

  // Getting super-user access to the CSPI
  CSPI_LIBPARAMS lib_params = {1, 1};
  rc = cspi_setlibparam(&lib_params, CSPI_LIB_SUPERUSER);
  HANDLE_SERVER_ERROR(rc,"CSPIConnect::cspi_setlibparam");
  t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
  cerr << conn.name << ":server_setparam() " << (t1-t0) << "ms " << GetDate() << endl;
#endif

  // Allocate handle and set env params
  t0 = CSPIHelper::GetTicks();
  rc = cspi_allochandle(CSPI_HANDLE_ENV, 0, &conn.envHandle);
  HANDLE_SERVER_ERROR(rc, "CSPIConnect::cspi_allochandle");
  t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
  cerr << conn.name << ":cspi_allochandle(CSPI_HANDLE_ENV) " << (t1-t0) << "ms " << GetDate() << endl;
#endif

  if(conn.flags) {

    // Aloocate handle
    t0 = CSPIHelper::GetTicks();
    rc = cspi_allochandle(CSPI_HANDLE_CON, conn.envHandle, &conn.conHandle);
    HANDLE_CSPI_ERROR(rc, "CSPIConnect::cspi_allochandle");
    t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
    cerr << conn.name << ":cspi_allochandle(CSPI_HANDLE_CON) " << (t1-t0) << "ms " << GetDate() << endl;
#endif

    // Set connection param
    t0 = CSPIHelper::GetTicks();
    bool SA_NONBLOCK = (conn.flags & CSPI_CON_SANONBLOCK)!=0;
    conn.flags &= ~CSPI_CON_SANONBLOCK;
    rc = cspi_setconparam(conn.conHandle, reinterpret_cast < CSPI_CONPARAMS * >(&conn.conParams), conn.flags);
    HANDLE_CSPI_ERROR(rc, "CSPIConnect::cspi_setconparam()");
    t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
    cerr << conn.name << ":cspi_setconparam(CSPI_CON_SANONBLOCK) " << (t1-t0) << "ms " << GetDate() << endl;
#endif

    // Connect
    t0 = CSPIHelper::GetTicks();
    rc = ::cspi_connect(conn.conHandle);
    HANDLE_CSPI_ERROR(rc, "CSPIConnect::cspi_connect");
    t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
    cerr << conn.name << ":cspi_connect() " << (t1-t0) << "ms " << GetDate() << endl;
#endif

    // Set SA non block (if asked)
    if(SA_NONBLOCK) {
      rc = cspi_setconparam(conn.conHandle, reinterpret_cast < CSPI_CONPARAMS * >(&conn.conParams), CSPI_CON_SANONBLOCK);
      HANDLE_CSPI_ERROR(rc, "CSPIConnect::cspi_setconparam()");
    }

  }

  return true;

}

// -------------------------------------------------------------------------------------------------------------

string CSPIHelper::GetServerError() {

  // Connection error
  switch(errno) {
    case SRV_E_PROTO:
      return "Protocol mismatch";
    case  SRV_E_INVAL:
      return "Invalid argument";
    default:
      return string(strerror(errno));
  }

}

// -------------------------------------------------------------------------------------------------------------

string CSPIHelper::GetCSPIError(int code) {

  return string(cspi_strerror(code));

}

// -------------------------------------------------------------------------------------------------------------
string CSPIHelper::GetDate() {

  char tmp[128];
  time_t now = time(NULL);
  struct tm *dt = localtime(&now);
  sprintf(tmp,"%02d/%02d/%04d %02d:%02d:%02d",dt->tm_mday,dt->tm_mon+1,dt->tm_year+1900,dt->tm_hour,dt->tm_min,dt->tm_sec);
  return string(tmp);

}

// -------------------------------------------------------------------------------------------------------------

void CSPIHelper::ClearConn(CSPI_CONN &conn) {

  memset(&conn.conParams ,0,sizeof(CSPI_CONPARAMS_EBPP));
  conn.envHandle = NULL;
  conn.conHandle = NULL;
  conn.flags = 0;
  conn.name = "No name";

}

// -------------------------------------------------------------------------------------------------------------


void CSPIHelper::CSPIDisconnect(CSPI_CONN &conn) {

  time_t t0;
  time_t t1;

  if( conn.conHandle ) {

    // Unregister callback if needed
    if( conn.conParams.event_mask & CSPI_EVENT_TRIGGET ) {
      conn.conParams.event_mask &= ~CSPI_EVENT_TRIGGET;

      t0 = CSPIHelper::GetTicks();
      cspi_setconparam(conn.conHandle, reinterpret_cast < CSPI_CONPARAMS * >(&conn.conParams), CSPI_CON_EVENTMASK);
      t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
      cerr << conn.name << ":cspi_setconparam(~TRIGET) " << (t1-t0) << "ms " << GetDate() << endl;
#endif
    }

    if( conn.conParams.event_mask & CSPI_EVENT_PM ) {
      conn.conParams.event_mask &= ~CSPI_EVENT_PM;

      t0 = CSPIHelper::GetTicks();
      cspi_setconparam(conn.conHandle, reinterpret_cast < CSPI_CONPARAMS * >(&conn.conParams), CSPI_CON_EVENTMASK);
      t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
      cerr << conn.name << ":cspi_setconparam(~PM) " << (t1-t0) << "ms " << GetDate() << endl;
#endif

    }

    t0 = CSPIHelper::GetTicks();
    cspi_disconnect(conn.conHandle);
    t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
    cerr << conn.name << ":cspi_disconnect() " << (t1-t0) << "ms " << GetDate() << endl;
#endif

    t0 = CSPIHelper::GetTicks();
    cspi_freehandle(CSPI_HANDLE_CON, conn.conHandle);
    t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
    cerr << conn.name << ":cspi_freehandle_con() " << (t1-t0) << "ms " << GetDate() << endl;
#endif

    conn.conHandle = NULL;

  }

  if( conn.envHandle ) {
    t0 = CSPIHelper::GetTicks();
    cspi_freehandle(CSPI_HANDLE_ENV, conn.envHandle);
    t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
    cerr << conn.name << ":cspi_freehandle() " << (t1-t0) << "ms " << GetDate() << endl;
#endif
    conn.envHandle = NULL;
  }

  t0 = CSPIHelper::GetTicks();
  server_disconnect();
  t1 = CSPIHelper::GetTicks();
#ifdef DPRINT
  cerr << conn.name << ":server_disconnect() " << (t1-t0) << "ms " << GetDate() << endl;
#endif

}

// -------------------------------------------------------------------------------------------------------------

time_t CSPIHelper::GetTicks() {

  static time_t tickStart = -1;
  if (tickStart < 0)
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv, NULL);
  return ((tv.tv_sec - tickStart) * 1000 + tv.tv_usec / 1000);

}
