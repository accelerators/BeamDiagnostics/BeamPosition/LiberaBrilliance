//+=============================================================================
//
// file :         ADCThread.cpp
//
// description :  Include for the ADCThread class.
//                This class is used for reading ADC data
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaBrilliance_ns
{
class ADCThread;
}

#include <ADCThread.h>
#include "CSPIHelper.h"

namespace LiberaBrilliance_ns
{

// CSPI Callback function
static int _cspi_callback( CSPI_EVENT *msg )
{
  ADCThread *th = (ADCThread *)msg->user_data;
  if(th->isConnected)
    th->wakeup();
  return 1;
}

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
ADCThread::ADCThread(LiberaBrilliance *libera, omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), ds(libera)
{
  INFO_STREAM << "ADCThread: Starting ADC Thread." << endl;

  trigger_cond = PTHREAD_COND_INITIALIZER;
  trigger_mutex = PTHREAD_MUTEX_INITIALIZER;
  threadStatus = "ADCThread: connecting";
  start_undetached();
}

// ----------------------------------------------------------------------------------------

void ADCThread::ResetConnection() {

  // Initialise connection parameters
  CSPIHelper::ClearConn(conn);
  conn.name = "ADC";
  conn.flags = CSPI_CON_MODE | CSPI_CON_EVENTMASK | CSPI_CON_HANDLER | CSPI_CON_USERDATA;
  conn.conParams.mode = CSPI_MODE_ADC;
  conn.conParams.event_mask = CSPI_EVENT_TRIGGET;
  conn.conParams.handler = _cspi_callback;
  conn.conParams.user_data = this;

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *ADCThread::run_undetached(void *arg) {

  time_t lastTime = 0;
  string errorStr;
  int rc;

  exitThread = false;
  isConnected = false;

  cout << "ADCThread: entering." << endl;

  while(!exitThread) {

    // Connection

    while (!isConnected && !exitThread) {

      ResetConnection();
      if (!CSPIHelper::CSPIConnect(conn, errorStr)) {
        // Connect failure
        mutex.lock();
        threadStatus = errorStr;
        mutex.unlock();
        CSPIHelper::CSPIDisconnect(conn);
        sleep(3);
      } else {
        isConnected = true;
      }

    }

    // Read data
    mutex.lock();
    threadStatus = "ADCThread: waiting";
    mutex.unlock();

    cout << "ADCThread: connected." << endl;

    while (!exitThread) {

      // Wait for data
      struct timeval now;
      struct timespec timeout;
      gettimeofday(&now, 0);

      // Wait for ever
      timeout.tv_sec = INT32_MAX;
      timeout.tv_nsec = now.tv_usec * 1000;

      rc = pthread_cond_timedwait(&trigger_cond,
                                  &trigger_mutex,
                                  &timeout);

      if (!rc) {

        if (!exitThread) {

          // Read ADC data
          size_t size = (size_t)ds->attr_ADC_BufSize_read[0];
          CSPI_ADC_ATOM *adcData = (CSPI_ADC_ATOM *)malloc(size*sizeof(CSPI_ADC_ATOM));
          rc = ReadData(adcData,size);

          if(rc) {

            // We may got a disconnection from the server
            // Retry once
            isConnected = false;
            CSPIHelper::CSPIDisconnect(conn);
            ResetConnection();
            if (CSPIHelper::CSPIConnect(conn, errorStr)) {
              isConnected = true;
              rc = ReadData(adcData,size);
            }

            if(rc) {

              cerr << "ADCThread::cspi_read_ex() failed " << CSPIHelper::GetCSPIError(rc) << " "
                   << CSPIHelper::GetDate() << endl;

              free(adcData);

              // Communication error, go to reconnection
              mutex.lock();
              threadStatus = "ADCThread: " + CSPIHelper::GetCSPIError(rc);
              mutex.unlock();
              isConnected = false;
              CSPIHelper::CSPIDisconnect(conn);

            } else {

              copyData(adcData,size);
              free(adcData);

            }

          } else {

            copyData(adcData,size);
            free(adcData);

          }


        }

      } else {

        cerr << "ADCThread: pthread_cond_timedwait failed: " << rc << endl;

      }

    }

  }

  cout << "ADCThread: disconnecting." << endl;

  isConnected = false;
  CSPIHelper::CSPIDisconnect(conn);

  cout << "ADCThread: exiting." << endl;
  return NULL;

}

// ----------------------------------------------------------------------------------------

int ADCThread::ReadData(CSPI_ADC_ATOM *adcData, size_t size) {

  int rc;

  size_t nbRead=0;

  rc = ::cspi_read_ex(conn.conHandle,
                      adcData,
                      size,
                      &nbRead,
                      0);

  if( nbRead!=size ) {
    return CSPI_W_INCOMPLETE;
  }

  return rc;

}

// ----------------------------------------------------------------------------------------

void ADCThread::copyData(CSPI_ADC_ATOM *adcData,size_t nb) {

  mutex.lock();
  adcValues.clear();
  for (int i = 0; i < (int)nb; i++) {
    ADC p;
    p.va = adcData[i].chA;
    p.vb = adcData[i].chB;
    p.vc = adcData[i].chC;
    p.vd = adcData[i].chD;
    adcValues.push_back(p);
  }
  mutex.unlock();

}

// ----------------------------------------------------------------------------------------

#define GET_VALUES(field)                      \
  int16_t *ret = NULL;                         \
  omni_mutex_lock l(mutex);                    \
  *length = adcValues.size();                  \
  if(adcValues.size()>0) {                     \
    ret = new int16_t[adcValues.size()];       \
    for(int i=0;i<(int)adcValues.size();i++)   \
      ret[i] = adcValues[i].field;             \
  }                                            \
  return ret;

int16_t *ADCThread::getVa(int *length) {
  GET_VALUES(va);
}
int16_t *ADCThread::getVb(int *length) {
  GET_VALUES(vb);
}
int16_t *ADCThread::getVc(int *length) {
  GET_VALUES(vc);
}
int16_t *ADCThread::getVd(int *length) {
  GET_VALUES(vd);
}


// ----------------------------------------------------------------------------------------

void ADCThread::wakeup() {
  // Wake up thread
  // cout << "ADCThread: wake up." << endl;
  pthread_cond_signal( &trigger_cond );
}

} // namespace LiberaBrilliance_ns
