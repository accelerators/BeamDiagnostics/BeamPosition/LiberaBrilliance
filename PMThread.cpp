//+=============================================================================
//
// file :         PMThread.cpp
//
// description :  Include for the PMThread class.
//                This class is used for reading PM data (Post mortem)
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
#define PM_BUF_SIZE 10000

namespace LiberaBrilliance_ns
{
class PMThread;
}

#include <PMThread.h>
#include "CSPIHelper.h"

namespace LiberaBrilliance_ns
{

// CSPI Callback function
static int _cspi_callback( CSPI_EVENT *msg )
{
  PMThread *th = (PMThread *)msg->user_data;
  if(th->isConnected)
    th->wakeup();
  return 1;
}

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
PMThread::PMThread(LiberaBrilliance *libera, PosCalculation *pc,omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), PC(pc), ds(libera)
{
  INFO_STREAM << "PMThread: Starting PM Thread." << endl;

  trigger_cond = PTHREAD_COND_INITIALIZER;
  trigger_mutex = PTHREAD_MUTEX_INITIALIZER;
  trigCount = 0;
  threadStatus = "PMThread: connecting";
  start_undetached();
}

// ----------------------------------------------------------------------------------------

void PMThread::ResetConnection() {

  // Initialise connection parameters
  CSPIHelper::ClearConn(conn);
  conn.name = "PM";
  conn.flags = CSPI_CON_MODE | CSPI_CON_EVENTMASK | CSPI_CON_HANDLER | CSPI_CON_USERDATA;
  conn.conParams.mode = CSPI_MODE_PM;
  conn.conParams.event_mask = CSPI_EVENT_PM;
  conn.conParams.handler = _cspi_callback;
  conn.conParams.user_data = this;

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *PMThread::run_undetached(void *arg) {

  time_t lastTime = 0;
  string errorStr;
  int rc;

  cout << "PMThread: entering." << endl;

  exitThread = false;
  isConnected = false;
  while(!exitThread) {

    // Connection

    while (!isConnected && !exitThread) {

      ResetConnection();
      if (!CSPIHelper::CSPIConnect(conn, errorStr)) {
        // Connect failure
        mutex.lock();
        threadStatus = errorStr;
        mutex.unlock();
        cout << errorStr << endl;
        CSPIHelper::CSPIDisconnect(conn);
        sleep(3);
      } else {
        isConnected = true;
      }

    }

    cout << "PMThread: connected." << endl;

    // Read data
    mutex.lock();
    threadStatus = "PMThread: waiting";
    mutex.unlock();

    while (isConnected && !exitThread) {

      // Wait for data
      struct timeval now;
      struct timespec timeout;
      gettimeofday(&now, 0);

      // Wait for ever
      timeout.tv_sec = INT32_MAX;
      timeout.tv_nsec = now.tv_usec * 1000;

      rc = pthread_cond_timedwait(&trigger_cond,
                                  &trigger_mutex,
                                  &timeout);

      if (!rc) {

        if (!exitThread) {

          if( !ds->attr_PM_Notified_read[0] ) {

            ds->attr_PM_Notified_read[0] = true;

            //unsigned long long offset = (unsigned long long)ds->attr_PM_Offset_read[0];
            unsigned long long offset = 0;
            rc = ::cspi_seek(conn.conHandle, &offset, CSPI_SEEK_TR);

            // Read PM data
            size_t size = (size_t) PM_BUF_SIZE;
            CSPI_DD_RAWATOM *ddData = (CSPI_DD_RAWATOM *) malloc(size * sizeof(CSPI_DD_RAWATOM));
            rc = ReadData(ddData, size);

            if (rc) {

              // We may got a disconnection from the server
              // Retry once
              isConnected = false;
              CSPIHelper::CSPIDisconnect(conn);
              ResetConnection();
              if (CSPIHelper::CSPIConnect(conn, errorStr)) {
                isConnected = true;
                rc = ReadData(ddData, size);
              }

              if (rc) {

                cerr << "PMThread::cspi_read_ex() failed " << CSPIHelper::GetCSPIError(rc) << " "
                     << CSPIHelper::GetDate()
                     << endl;

                free(ddData);
                // Communication error
                mutex.lock();
                threadStatus = "PMThread: " + CSPIHelper::GetCSPIError(rc);
                ddValues.clear();
                mutex.unlock();
                isConnected = false;
                CSPIHelper::CSPIDisconnect(conn);

              } else {

                computePosition(ddData, size);
                free(ddData);

              }

            } else {

              computePosition(ddData, size);
              free(ddData);

            }

          }

        }

      } else {

        cerr << "PMThread: pthread_cond_timedwait failed: " << rc << endl;

      }

    }

  }

  cout << "PMThread: disconnecting." << endl;

  isConnected = false;
  CSPIHelper::CSPIDisconnect(conn);

  cout << "PMThread: exiting." << endl;
  return NULL;

}

int PMThread::ReadData(CSPI_DD_RAWATOM *ddData, size_t size) {

  int rc;

  size_t nbRead=0;
  rc = ::cspi_read_ex(conn.conHandle, ddData, size, &nbRead, 0);

  //cout << "DDThread: Got " << nbRead << endl;

  //- <cspi_read_ex> special error case: err:-5 and errno:61: no data available
  //----------------------------------------------------------------------------
  //- this might occur when usign a huge DD buffer size. in this case reading
  //- DD data on Libera side is VERY slow and DD data might be overwritten in
  //- the circular DD data buffer on Libera side (or something similar). let's
  //- ignore this error...

  if (rc == CSPI_W_INCOMPLETE) {

    // From time to time, few samples can be missed at the end of the buffer
    // Fill with 0

    size_t missing_samples = size - nbRead;
    size_t start_fill = size - missing_samples;

    //cout << "DDThread: Adjusting nb of samples, adding " << missing_samples << " samples\n";

    for (size_t a = start_fill; a < size; a++) {
      ::memset(&(ddData[a]), 0, sizeof(CSPI_DD_RAWATOM));
    }
    nbRead = size;
    rc = 0;

  }

  return rc;

}

// ----------------------------------------------------------------------------------------

#define GET_VALUES(field)                      \
  double *ret = NULL;                          \
  omni_mutex_lock l(mutex);                    \
  *length = ddValues.size();                   \
  if(ddValues.size()>0) {                      \
    ret = new double[ddValues.size()];         \
    for(int i=0;i<(int)ddValues.size();i++)    \
      ret[i] = ddValues[i].field;              \
  }                                            \
  return ret;

double *PMThread::getVa(int *length) {
  GET_VALUES(va);
}
double *PMThread::getVb(int *length) {
  GET_VALUES(vb);
}
double *PMThread::getVc(int *length) {
  GET_VALUES(vc);
}
double *PMThread::getVd(int *length) {
  GET_VALUES(vd);
}
double *PMThread::getSum(int *length) {
  GET_VALUES(sum);
}
double *PMThread::getQ(int *length) {
  GET_VALUES(q);
}
double *PMThread::getH(int *length) {
  GET_VALUES(h);
}
double *PMThread::getV(int *length) {
  GET_VALUES(v);
}
double *PMThread::getIa(int *length) {
  GET_VALUES(ia);
}
double *PMThread::getIb(int *length) {
  GET_VALUES(ib);
}
double *PMThread::getIc(int *length) {
  GET_VALUES(ic);
}
double *PMThread::getId(int *length) {
  GET_VALUES(id);
}
double *PMThread::getQa(int *length) {
  GET_VALUES(qa);
}
double *PMThread::getQb(int *length) {
  GET_VALUES(qb);
}
double *PMThread::getQc(int *length) {
  GET_VALUES(qc);
}
double *PMThread::getQd(int *length) {
  GET_VALUES(qd);
}

// ----------------------------------------------------------------------------------------

void PMThread::computePosition(CSPI_DD_RAWATOM *dd,size_t nb) {

  mutex.lock();
  ddValues.clear();
  for (int i = 0; i < (int)nb; i++) {
    BP p;
    p.va = CORDIC(dd[i].sinVa, dd[i].cosVa);
    p.vb = CORDIC(dd[i].sinVb, dd[i].cosVb);
    p.vc = CORDIC(dd[i].sinVc, dd[i].cosVc);
    p.vd = CORDIC(dd[i].sinVd, dd[i].cosVd);
    PC->ComputePosition(dd[i],p,true);
    ddValues.push_back(p);
  }
  mutex.unlock();

}

// ----------------------------------------------------------------------------------------

unsigned int PMThread::getTriggerCounter() {
  return trigCount;
}

// ----------------------------------------------------------------------------------------

void PMThread::wakeup() {
  // Wake up thread
  //cout << "PMThread: wake up." << endl;
  trigCount++;
  pthread_cond_signal( &trigger_cond );
}

} // namespace LiberaBrilliance_ns
