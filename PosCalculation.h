//+=============================================================================
//
// file :         PositionCalculation.h
//
// description :  Include for the PositionCalculation class.
//                This class is used for computing beam position
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLIANCE_POSCALCULATION_H
#define LIBERABRILLIANCE_POSCALCULATION_H

#include "LiberaBrilliance.h"

#define DOS_CALC 0
#define POLYNOMIAL_CALC 1

#define MAX_ORDER      15  // Maximum polynomial order

namespace LiberaBrilliance_ns {

class PosCalculation {

public:

    PosCalculation(double st);
    void ComputePosition(CSPI_SA_ATOM &sa, BP &p);
    void ComputePosition(CSPI_DD_RAWATOM &dd, BP &p, bool loadIQ);
    void SetAlgorithm(int algo);
    int  GetAlgorithm();
    void SetTiltAngle(double angle);
    bool CheckPolynomial(vector<double> &hCoefs,string &error);
    void SetPolynomial(vector<POLY>& hPoly,vector<POLY>& vPoly,vector<POLY>& qPoly,vector<POLY>& sPoly);

    double kA;
    double kB;
    double kC;
    double kD;
    double KH;
    double KV;
    double OH;
    double OV;
    double OQ;

    double tiltAngle;

private:

    void ComputePosition(BP &p);
    double sumSAThreshold;
    int calculationAlgorithm;

    // Polynomial stuff
    vector<POLY> hPoly;
    vector<POLY> vPoly;
    vector<POLY> qPoly;
    vector<POLY> sPoly;

    double px[MAX_POLYNOMIAL_POWER];
    double py[MAX_POLYNOMIAL_POWER];
    int maxXPow;
    int maxYPow;

    double r11,r12,r21,r22;

};


static inline int CORDIC( int  I, int  Q ) {

    //- The number of iterations = CORDIC_MAXLEVEL + 1
    static const int CORDIC_MAXLEVEL = 11;

    //- CORDIC gain correction factor associated with the CORDIC_MAXLEVEL.
    static const int64_t CORDIC_GAIN64 = 2608131600LL; // (1/CG)<<32

    int tmp_I;
    int L = 0;

    if (I < 0) {
        tmp_I = I;
        if (Q > 0) {
            I = Q;
            Q = -tmp_I;     // Rotate by -90 degrees
        } else {
            I = -Q;
            Q = tmp_I;      // Rotate by +90 degrees
        }
    }

    for (; L <= CORDIC_MAXLEVEL; ++L) {
        tmp_I = I;
        if (Q >= 0) {
            // Positive phase; do negative rotation
            I += (Q >> L);
            Q -= (tmp_I >> L);
        } else {
            // Negative phase; do positive rotation
            I -= (Q >> L);
            Q += (tmp_I >> L);
        }
    }

#define CORDIC_IGNORE_GAIN
#if defined(CORDIC_IGNORE_GAIN)
    return I;
#else
    return (int)((CORDIC_GAIN64 * I) >> 32);
#endif

}

} // LiberaBrilliance_ns

#endif //LIBERABRILLIANCE_POSCALCULATION_H
