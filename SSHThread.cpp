//+=============================================================================
//
// file :         SSHThread.cpp
//
// description :  Include for the SSHThread class.
//                This class is used for executing ssh command on the brilliance
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaBrilliance_ns
{
class SSHThread;
}

#include <SSHThread.h>

namespace LiberaBrilliance_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
SSHThread::SSHThread(LiberaBrilliance *libera, omni_mutex &m, int f):
        Tango::LogAdapter(libera), mutex(m), ds(libera), command(f)
{
  INFO_STREAM << "SSHThread: Starting SSH Thread." << endl;
  output.clear();
  start();
}

// ----------------------------------------------------------------------------------------

string SSHThread::getOutput() {

  string ret;

  mutex.lock();
  for(int i=0;i<(int)output.size();i++) {
    ret.append(output[i]);
    ret.append("\n");
  }
  mutex.unlock();

  return ret;

}

// ----------------------------------------------------------------------------------------

void SSHThread::readLine(char *buffer,int buffLength) {

  memcpy(innerBuffer+innerBufferSize,buffer,buffLength);
  innerBufferSize += buffLength;

  int bPos = 0;

  while( bPos<innerBufferSize ) {

    while (innerBuffer[bPos] >= 32 && bPos < innerBufferSize)
      bPos++;

    if(bPos<innerBufferSize) {
      // End of line reached
      string ret;
      for(int i=0;i<bPos;i++)
        ret.push_back(innerBuffer[i]);
      outputString(ret);
      bPos++;
      innerBufferSize -= bPos;
      memcpy(innerBuffer,innerBuffer+bPos,innerBufferSize);
      bPos = 0;
    }

  }

}

// ----------------------------------------------------------------------------------------

void SSHThread::flush() {

  if( innerBufferSize>0 ) {
    string ret;
    for (int i = 0; i < innerBufferSize; i++)
      ret.push_back(innerBuffer[i]);
    outputString(ret);
  }

}

// ----------------------------------------------------------------------------------------

void SSHThread::outputString(std::string str) {

  mutex.lock();
  output.push_back(str);
  cout << str << endl;
  mutex.unlock();

}

// ----------------------------------------------------------------------------------------

static int ssh_auth_callback(const char *prompt, char *buf, size_t len,
                         int echo, int verify, void *userdata) {

  SSHThread *obj = (SSHThread *)userdata;
  obj->outputString("SSHThread: ssh_auth_callback()");
  obj->outputString(string(prompt));
  return 0;

}

// ----------------------------------------------------------------------------------------

void SSHThread::disconnect() {

  cout << "SSHThread: disconnecting." << endl;

  if(channel) {
    ssh_channel_close(channel);
    ssh_channel_free(channel);
    channel = NULL;
  }

  if(session) {
    ssh_disconnect(session);
    ssh_free(session);
    session = NULL;
  }


  ds->sshRunning = false;

  cout << "SSHThread: exiting." << endl;

}

// ----------------------------------------------------------------------------------------

bool SSHThread::ssh_execute(string command) {

  cout << "ssh_execute(): " << command << endl;

  // ----------------------------------------------------------------------------------------
  // Channel
  channel = ssh_channel_new(session);
  if (channel == NULL) {
    outputString("SSH: Channel not created " + ds->liberaIpAddr + ", " + ssh_get_error(session));
    disconnect();
    return false;
  }

  int rc = ssh_channel_open_session(channel);
  if (rc != SSH_OK)
  {
    outputString("SSH: Cannot open session " + ds->liberaIpAddr + ", " + ssh_get_error(session));
    disconnect();
    return false;
  }

  // ----------------------------------------------------------------------------------------
  // Exec
  rc = ssh_channel_request_exec(channel, command.c_str());
  if (rc != SSH_OK)
  {
    outputString("SSH: Error sending command to " + ds->liberaIpAddr + ", " + ssh_get_error(session));
    disconnect();
    return false;
  }

  outputString("SSH: " + command);

  // ----------------------------------------------------------------------------------------
  // Read output

  innerBufferSize = 0;
  char buffer[1024];
  int nbBytes;
  nbBytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
  while (nbBytes > 0) {
    readLine(buffer,nbBytes);
    nbBytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
  }

  if(nbBytes==0)
    flush();

  /*
  if (nbBytes < 0)  {
    outputString("SSH: Error executing command on " + ds->liberaIpAddr + " " + ssh_get_error(session));
    disconnect();
    return false;
  }
  */

  ssh_channel_send_eof(channel);
  ssh_channel_close(channel);
  ssh_channel_free(channel);
  channel = NULL;

  return true;

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void SSHThread::run(void *arg) {

  cout << "SSHThread: entering." << endl;

  session = NULL;
  channel = NULL;

  int rc;
  session = ssh_new();
  if (session == NULL) {
    outputString("Cannot open ssh session, ssh_new() failed");
    cout << "SSHThread: exiting." << endl;
    return;
  }

  // ----------------------------------------------------------------------------------------
  // Session

  if( ssh_options_set(session, SSH_OPTIONS_HOST, ds->liberaIpAddr.c_str())<0 ) {
    outputString("SSH: ssh_options_set(SSH_OPTIONS_HOST," + ds->liberaIpAddr + ") " + ssh_get_error(session));
    disconnect();
    return;
  };

  if(ssh_options_set(session, SSH_OPTIONS_USER, "root")<0) {
    outputString("SSH: ssh_options_set(SSH_OPTIONS_USER,root) " + string(ssh_get_error(session)));
    disconnect();
    return;
  }

  int yesno = false;
  if(ssh_options_set(session, SSH_OPTIONS_STRICTHOSTKEYCHECK, &yesno)<0) {
    outputString("SSH: ssh_options_set(SSH_OPTIONS_STRICTHOSTKEYCHECK,root) " + string(ssh_get_error(session)));
    disconnect();
    return;
  }

  //struct ssh_callbacks_struct cb;
  //cb.auth_function = ssh_auth_callback;
  //cb.userdata=this;
  //ssh_callbacks_init(&cb);
  //ssh_set_callbacks(session,&cb);

  ssh_options_parse_config(session,NULL);

  rc = ssh_connect(session);
  if (rc != SSH_OK)
  {
    outputString("Error connecting to " + ds->liberaIpAddr + ", " + ssh_get_error(session));
    disconnect();
    return;
  }

  outputString("SSH: Connected to " + ds->liberaIpAddr);

  ssh_userauth_none(session, NULL);
  ssh_userauth_password(session, NULL, "Jungle");

  // ----------------------------------------------------------------------------------------

  switch(command) {
    case SSH_STD_FIRMWARE:
    case SSH_MAF_FIRMWARE:
      change_MAF(command);
      break;
    case SSH_RESTART_DAEMON:
      restart_daemon();
      break;
  }

  // ----------------------------------------------------------------------------------------

  disconnect();

  cout << "SSHThread: exiting." << endl;

}

void SSHThread::restart_daemon() {

  ssh_execute("start-stop-daemon --stop --quiet --pidfile /var/run/leventd.pid");
  sleep(3);
  ssh_execute("start-stop-daemon --start --quiet --exec /opt/bin/leventd");
  sleep(3);

}

void SSHThread::change_MAF(int firmware) {

  // Stop the libera software
  ssh_execute("/etc/init.d/libera stop");

  // Wait a bit
  sleep(3);

  // Mount as read write
  ssh_execute("mount -o remount,rw /");

  // Make the link to the appropriate firmware
  switch(firmware) {
    case SSH_STD_FIRMWARE:
      ssh_execute("ln -fs /opt/lib/main_esrf_sr_STD.bin /opt/lib/main_esrf_sr.bin");
      break;
    case SSH_MAF_FIRMWARE:
      ssh_execute("ln -fs /opt/lib/main_esrf_sr_MAF.bin /opt/lib/main_esrf_sr.bin");
      break;
    default:
      outputString("Wrong firmware !");
  }

  // Restore read only mode
  ssh_execute("mount -o remount,ro /");

  // Retstart the libera software
  ssh_execute("/etc/init.d/libera start");

}

} // namespace LiberaBrilliance_ns
