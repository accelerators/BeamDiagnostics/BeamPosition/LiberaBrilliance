//+=============================================================================
//
// file :         ConfigThread.cpp
//
// description :  This class is used for Brilliance configuation
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaBrilliance_ns
{
class ConfigThread;
}

#include <ConfigThread.h>
#include <ITECH/esrf-brilliance-update-2.25/src/cspi-2.25/src/ebpp.h>
#include <ITECH/esrf-brilliance-update-2.25/src/driver-2.25/src/libera.h>
#include "CSPIHelper.h"
#include "ConfigThread.h"

#define allMask (CSPI_ENV_HEALTH |\
        CSPI_ENV_TRIGMODE |\
        CSPI_ENV_PLL |\
        CSPI_ENV_FEATURE |\
        CSPI_ENV_KX |\
        CSPI_ENV_KY |\
        CSPI_ENV_XOFFSET |\
        CSPI_ENV_YOFFSET |\
        CSPI_ENV_QOFFSET |\
        CSPI_ENV_SWITCH |\
        CSPI_ENV_GAIN  |\
        CSPI_ENV_AGC |\
        CSPI_ENV_DSC |\
        CSPI_ENV_ILK  |\
        CSPI_ENV_ILKSTATUS  |\
        CSPI_ENV_PMOFFSET  |\
        CSPI_ENV_TRIGDELAY  |\
        CSPI_ENV_EXTSWITCH  |\
        CSPI_ENV_SWDELAY  |\
        CSPI_ENV_NOTCH1  |\
        CSPI_ENV_NOTCH2 |\
        CSPI_ENV_POLYPHASE_FIR  |\
        CSPI_ENV_DDC_MAFLENGTH  |\
        CSPI_ENV_DDC_MAFDELAY |\
        CSPI_ENV_MTVCXOFFS |\
        CSPI_ENV_MTNCOSHFT |\
        CSPI_ENV_MTPHSOFFS |\
        CSPI_ENV_MTUNLCKTR  |\
        CSPI_ENV_MTSYNCIN |\
        CSPI_ENV_STUNLCKTR |\
        CSPI_ENV_LPLLDSTAT |\
        CSPI_ENV_AVERAGE_SUM |\
        CSPI_ENV_PM |\
        CSPI_ENV_SR |\
        CSPI_ENV_SP |\
        CSPI_ENV_FREV |\
        CSPI_ENV_PMDEC |\
        CSPI_ENV_DSCCOEFF |\
        CSPI_ENV_STATS_SA |\
        CSPI_ENV_STATS_DD  |\
        CSPI_ENV_STATS_WINDOW  |\
        CSPI_ENV_LIFETIME_WINDOW  |\
        CSPI_ENV_LIFETIME |\
        CSPI_ENV_STATS_DD_DEC |\
        CSPI_ENV_DELTA_SUM_COEFF |\
        CSPI_ENV_DELTA_SUM_CONTROL |\
        CSPI_ENV_DELTA_SUM_STATUS)

// Refresh period (in us)
#define CONFIG_LOOP_PERIOD 2000000


// Special definition for extra timing command
#define CSPI_ENV_PMMODE CUSTOM_ENV_BIT(52)
#define CSPI_ENV_STARTCOMCONTROLLER CUSTOM_ENV_BIT(53)

namespace LiberaBrilliance_ns {


// ----------------------------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------------------------
ConfigThread::ConfigThread(LiberaBrilliance *libera, omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), ds(libera)
{

  INFO_STREAM << "ConfigThread: Starting SA Thread." << endl;

  init();
  start_undetached();

}

ConfigThread::ConfigThread(LiberaBrilliance *libera, omni_mutex &m,void *backup):
         Tango::LogAdapter(libera), mutex(m), ds(libera)
{

  init();
  initBackup = backup;
  start_undetached();

}

void ConfigThread::init() {

  // Initialise connection parameters
  CSPIHelper::ClearConn(conn);
  conn.name = "ConfigThread";
  memset(&params,0, sizeof(CSPI_ENVPARAMS));
  command_cond = PTHREAD_COND_INITIALIZER;
  command_mutex = PTHREAD_MUTEX_INITIALIZER;

  threadStatus = "ConfigThread: connecting";
  exitThread = false;
  isConnected = false;
  initBackup = NULL;
  firstConnect = true;

}

void *ConfigThread::getBackup() {
  void *ret = malloc(sizeof(CSPI_ENVPARAMS));
  memcpy(ret,&params_backup,sizeof(CSPI_ENVPARAMS));
  return ret;
}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *ConfigThread::run_undetached(void *arg) {

  string errorStr;
  int rc;

  cout << "ConfigThread: entering." << endl;

  while(!exitThread) {

    // Connection
    while(!isConnected && !exitThread) {

      if( !CSPIHelper::CSPIConnect(conn,errorStr) ) {
        // Connect failure
        mutex.lock();
        threadStatus = errorStr;
        mutex.unlock();
        CSPIHelper::CSPIDisconnect(conn);
        sleep(3);
      } else {

        sleep(10);

        // Write default value
        rc = write_default();

        // Read environement
        rc = dump_env_parameters();

        if (!rc) {
          if(!firstConnect) {
            // Reconnection, restore environement
            cerr << "Reconnecting to CSPI" << endl;
            restore_backup();
          } else {
            if( initBackup!=NULL ) {
              // Restore given backup
              memcpy(&params_backup,initBackup,sizeof(CSPI_ENVPARAMS));
              restore_backup();
              free(initBackup);
              initBackup = NULL;
            }
          }
          firstConnect = false;
          isConnected = true;
        } else {
          mutex.lock();
          threadStatus = "ConfigThread: " + CSPIHelper::GetCSPIError(rc);
          mutex.unlock();
          CSPIHelper::CSPIDisconnect(conn);
          sleep(3);
        }
      }

    }

    mutex.lock();
    threadStatus = "ConfigThread: running";
    mutex.unlock();

    while(isConnected && !exitThread) {


      time_t t0 = CSPIHelper::GetTicks();

      // Read environement
      CSPI_BITMASK refreshMask = allMask;
      int rc = cspi_getenvparam(conn.envHandle,&params,refreshMask);

      if( rc ) {

        // Communication error, go to reconnection
        cerr << "ConfigThread::cspi_getenvparam() failed " << CSPIHelper::GetCSPIError(rc) << " " << CSPIHelper::GetDate() << endl;

        mutex.lock();
        threadStatus = "ConfigThread: " + CSPIHelper::GetCSPIError(rc);
        mutex.unlock();
        CSPIHelper::CSPIDisconnect(conn);
        isConnected = false;

      }

      // Read FA Info
      rc = ::cspi_getenvparam_fa(conn.envHandle,0xC00,FAIParams,4,34);

      if( rc ) {

        // Communication error, go to reconnection
        cerr << "ConfigThread::cspi_getenvparam_fa() failed " << CSPIHelper::GetCSPIError(rc) << " " << CSPIHelper::GetDate() << endl;

        mutex.lock();
        threadStatus = "ConfigThread: " + CSPIHelper::GetCSPIError(rc);
        mutex.unlock();
        CSPIHelper::CSPIDisconnect(conn);
        isConnected = false;

      }

      time_t t1 = CSPIHelper::GetTicks();

      ds->attr_Time_Env_read[0] = (t1-t0);

      if( isConnected ) {

        // Backup env params
        memcpy(&params_backup,&params,sizeof(CSPI_ENVPARAMS));

        // Execute all pendings commands
        while (get_command_count())
          execute_command();

        // Wait for new commands during the 'sleeping time'
        struct timeval now;
        struct timespec timeout;
        gettimeofday(&now, 0);

        int delayus = (int)(CONFIG_LOOP_PERIOD-(t1-t0)*1000);

        while( delayus>0 ) {

          // Offset timeout 'delay' seconds in the future.
          time_t nbuSec = now.tv_sec * 1000000 +  now.tv_usec + delayus;
          timeout.tv_sec = nbuSec / 1000000;
          timeout.tv_nsec = (nbuSec % 1000000) * 1000;

          pthread_cond_timedwait(&command_cond,
                                 &command_mutex,
                                 &timeout);

          // Execute all pendings commands
          while (get_command_count())
            execute_command();

          t1 = CSPIHelper::GetTicks();
          delayus = (int)(CONFIG_LOOP_PERIOD-(t1-t0)*1000);

        }

      }

    }

  }

  mutex.lock();
  threadStatus = "ConfigThread: off";
  mutex.unlock();

  cout << "ConfigThread: disconnecting." << endl;

  CSPIHelper::CSPIDisconnect(conn);

  cout << "ConfigThread: exiting." << endl;

  return NULL;

}

// ----------------------------------------------------------------------------------------

double ConfigThread::getKH() {
  if(!isConnected) return NAN;
  return (double)params.Kx;
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getKV() {
  if(!isConnected) return NAN;
  return (double)params.Ky;
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getOffsetH() {
  if(!isConnected) return NAN;
  return (double)params.Xoffset;
}

void ConfigThread::setOffsetH(double offsetH) {
  add_command("SetHOffset",CSPI_ENV_XOFFSET,-offsetH);
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getOffsetV() {
  if(!isConnected) return NAN;
  return (double)params.Yoffset;
}

void ConfigThread::setOffsetV(double offsetV) {
  add_command("SetVOffset",CSPI_ENV_YOFFSET,-offsetV);
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getOffsetQ() {
  if(!isConnected) return NAN;
  return (double)params.Qoffset;
}

void ConfigThread::setOffsetQ(double offsetQ) {
  add_command("SetQOffset",CSPI_ENV_QOFFSET,offsetQ);
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getHWTemp1() {
  if(!isConnected) return NAN;
  return (double)params.health.temp[0];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getHWTemp2() {
  if(!isConnected) return NAN;
  return (double)params.health.temp[1];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getHWTemp3() {
  if(!isConnected) return NAN;
  return (double)params.health.temp[2];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getFan1Speed() {
  if(!isConnected) return NAN;
  return (double)params.health.fan[0];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getFan2Speed() {
  if(!isConnected) return NAN;
  return (double)params.health.fan[1];
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getExternalTriggerDelay() {
  if(!isConnected) return 0;
  return params.trig_delay;
}

void ConfigThread::setExternalTriggerDelay(int delay) {
  add_command("SetTrigDelay",CSPI_ENV_TRIGDELAY,delay);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getPMOffset() {
  if(!isConnected) return 0;
  return params.PMoffset;
}

void ConfigThread::setPMOffset(int offset) {
  add_command("SetPMOffset",CSPI_ENV_PMOFFSET,offset);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getPMDecFactor() {
  if(!isConnected) return 0;
  return params.PMdec;
}

void ConfigThread::setPMDecFactor(int factor) {
  add_command("SetPMDecFactor",CSPI_ENV_PMDEC,factor);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getPMMode() {
  if(!isConnected) return 0;
  return params.pm.mode;
}

void ConfigThread::setPMMode(int mode) {
  add_command("SetPMMode",CSPI_ENV_PMMODE,mode);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getSwitches() {
  if(!isConnected) return 0;
  if( params.switches==255 ) {
    return 16;
  } else {
    return params.switches;
  }
}

void ConfigThread::setSwitches(int switches) {
  if(switches==16) {
    add_command("SetSwitches", CSPI_ENV_SWITCH, 255);
  } else {
    add_command("SetSwitches", CSPI_ENV_SWITCH, switches);
  }
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getSwitchingDelay() {
  if(!isConnected) return 0;
  return params.switching_delay;
}

void ConfigThread::setSwitchingDelay(int delay) {
  add_command("SetSwitchingDelay",CSPI_ENV_SWDELAY,delay);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getExternalSwitching() {
  if(!isConnected) return 0;
  return params.external_switching;
}

void ConfigThread::setExternalSwitching(int external) {
  add_command("SetExternalSwitching",CSPI_ENV_EXTSWITCH,external);
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getAvgSumBetweenTrigger() {
  if(!isConnected) return NAN;
  return params.average_sum;
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getDSCMode() {
  if(!isConnected) return 0;
  return params.dsc;
}

void ConfigThread::setDSCMode(int mode) {
  add_command("SetDSCMode",CSPI_ENV_DSC,mode);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getDSCStatus() {
  if(!isConnected) return 0;
  return params.dsccoeff[0];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getDSCCoefA() {
  if(!isConnected) return NAN;
  return params.dsccoeff[1];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getDSCCoefB() {
  if(!isConnected) return NAN;
  return params.dsccoeff[2];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getDSCCoefC() {
  if(!isConnected) return NAN;
  return params.dsccoeff[3];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getDSCCoefD() {
  if(!isConnected) return NAN;
  return params.dsccoeff[4];
}

// ----------------------------------------------------------------------------------------

double ConfigThread::getGain() {
  if(!isConnected) return NAN;
  return params.gain;
}

void ConfigThread::setGain(double gain) {
  add_command("SetGain",CSPI_ENV_GAIN,gain);
}

// ----------------------------------------------------------------------------------------

bool ConfigThread::getAGC() {
  if(!isConnected) return false;
  return params.agc;
}

void ConfigThread::setAGC(bool agc) {
  add_command("SetAGC",CSPI_ENV_AGC,agc);
}

// ----------------------------------------------------------------------------------------

void ConfigThread::resetItlk() {
  add_command("ResetInterlock",CSPI_ENV_ILKSTATUS,0);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getOffsetTune() {
  if(!isConnected) return 0;
  return params.mtvcxoffs;
}

void ConfigThread::setOffsetTune(int tune) {
  add_command("SetOffsetTune",CSPI_ENV_MTVCXOFFS,tune);
}

// ----------------------------------------------------------------------------------------

bool ConfigThread::getCompensateTune() {
  if(!isConnected) return 0;
  return params.mtncoshft;

}

void ConfigThread::setCompensateTune(bool compensate) {
  add_command("setCompensateTune",CSPI_ENV_MTNCOSHFT,compensate);
}

// ----------------------------------------------------------------------------------------

bool ConfigThread::getMCPLLStatus() {
  if(!isConnected) return false;
  return params.pll.mc;
}

// ----------------------------------------------------------------------------------------

bool ConfigThread::getSCPLLStatus() {
  if(!isConnected) return false;
  return params.pll.sc;
}

// ----------------------------------------------------------------------------------------

string ConfigThread::getMTStatus() {

  if(!isConnected) return "Not connected";

  switch(params.pll_status.mt_stat.status) {
    case 0:
      return "OK";
    case 1:
      return "Not yet initialised";
    case 2:
      return "No external clock signal";
    case 3:
      return "Phase error too large";
    case 4:
      return "Terminated";
  }

  return "Unknown #" + to_string(params.pll_status.mt_stat.status);

}

// ----------------------------------------------------------------------------------------

vector<string> ConfigThread::getInterlockConfig() {

  vector<string> ret;

  if(!isConnected) return ret;


  switch (params.ilk.mode) {
    case 0: ret.push_back("Mode: Disabled"); break;
    case 1: ret.push_back("Mode: Enabled"); break;
    case 3: ret.push_back("Mode: Gain Dependant"); break;
    default:ret.push_back("Mode: Unknown mode " + to_string(params.ilk.mode));
  }

  ret.push_back("H low: " + to_string((double)params.ilk.Xlow / 1e6) + " mm");
  ret.push_back("H high: " + to_string((double)params.ilk.Xhigh / 1e6) + " mm");
  ret.push_back("V low: " + to_string((double)params.ilk.Ylow / 1e6) + " mm");
  ret.push_back("V high: " + to_string((double)params.ilk.Yhigh / 1e6) + " mm");
  ret.push_back("ADC underflow: " + to_string(params.ilk.underflow_limit));
  ret.push_back("ADC overflow: " + to_string(params.ilk.overflow_limit));
  ret.push_back("ADC overflow duration: " + to_string(params.ilk.overflow_dur) + " samples");
  ret.push_back("Gain (inhibit interlock under): " + to_string(params.ilk.gain_limit) + " dbM");

  return ret;

}

// ----------------------------------------------------------------------------------------

bool ConfigThread::isInterlockEnabled() {
  if(!isConnected) return false;
  return params.ilk.mode>0;
}
bool ConfigThread::getItlkH() {
  if(!isConnected) return false;
  return INTERLOCK_XPOSITION(params.ilk_status);
}
bool ConfigThread::getItlkV() {
  if(!isConnected) return false;
  return INTERLOCK_YPOSITION(params.ilk_status);
}
bool ConfigThread::getItlkAtt() {
  if(!isConnected) return false;
  return INTERLOCK_ATT_LIMIT(params.ilk_status);
}
bool ConfigThread::getItlkADCOverflowNoFilter() {
  if(!isConnected) return false;
  return INTERLOCK_ADC_NOT_FILTERED(params.ilk_status);
}
bool ConfigThread::getItlkADCOverflowFilter() {
  if(!isConnected) return false;
  return INTERLOCK_ADC_OVERFLOW(params.ilk_status);
}
bool ConfigThread::getItlkADCUnderflow() {
  if(!isConnected) return false;
  return INTERLOCK_ADC_UNDERFLOW(params.ilk_status);
}

// ----------------------------------------------------------------------------------------

bool ConfigThread::hasMAF() {
  if(!isConnected) return false;
  return LIBERA_IS_MAF(params.feature.itech);
}

void ConfigThread::setMAFLength(int length) {
  add_command("SetMAFDelay",CSPI_ENV_DDC_MAFLENGTH,length);
}

void ConfigThread::setMAFDelay(int delay) {
  add_command("SetMAFDelay",CSPI_ENV_DDC_MAFDELAY,delay);
}

int ConfigThread::getMAFLength() {
  if(!isConnected) return 0;
  return params.ddc_maflength;
}

int ConfigThread::getMAFDelay() {
  if(!isConnected) return 0;
  return params.ddc_mafdelay;
}

// ----------------------------------------------------------------------------------------

void ConfigThread::setTrigMode(int trigMode) {
  add_command("SetTrigMode",CSPI_ENV_TRIGMODE,trigMode);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::getDCCProcessTime() {
  if(!isConnected) return 0;
  return FAIParams[28];
}

int *ConfigThread::getFAIParams() {
  if(!isConnected) return NULL;
  return FAIParams;
}

// ----------------------------------------------------------------------------------------

void ConfigThread::startComController() {
  add_command("SetTrigMode",CSPI_ENV_STARTCOMCONTROLLER,0);
}

// ----------------------------------------------------------------------------------------

int ConfigThread::write_default() {

  CSPI_BITMASK change = 0;
  // Default value
  change |= CSPI_ENV_KX;
  params.Kx = ds->kH;
  change |= CSPI_ENV_KY;
  params.Ky = ds->kV;
  change |= CSPI_ENV_AGC;
  params.agc = false;

  int rc = cspi_setenvparam(conn.envHandle, &params, change);
  if (rc)
    cerr << "write_default() " << CSPIHelper::GetCSPIError(rc) << endl;

  cout << "SetKX(" << params.Kx << ") ok !" << endl;
  cout << "SetKY(" << params.Ky << ") ok !" << endl;

  return rc;

}

// ----------------------------------------------------------------------------------------

#define RESTORE(field,mask)                \
if(params.field != params_backup.field ) { \
  change |= mask;                          \
  params.field = params_backup.field;      \
  cout << "Restoring " << #mask << ": " << params.field << endl;   \
}

int ConfigThread::restore_backup() {

  // Restore value after reconnect

  CSPI_BITMASK change = 0;

  RESTORE(Kx,CSPI_ENV_KX);
  RESTORE(Ky,CSPI_ENV_KY);
  RESTORE(Xoffset,CSPI_ENV_XOFFSET);
  RESTORE(Yoffset,CSPI_ENV_YOFFSET);
  RESTORE(trig_delay,CSPI_ENV_TRIGDELAY);
  RESTORE(PMoffset,CSPI_ENV_PMOFFSET);
  RESTORE(PMdec,CSPI_ENV_PMDEC);
  RESTORE(switches,    CSPI_ENV_SWITCH);
  RESTORE(switching_delay, CSPI_ENV_SWDELAY);
  RESTORE(external_switching,CSPI_ENV_EXTSWITCH);
  RESTORE(dsc,CSPI_ENV_DSC);
  RESTORE(gain,CSPI_ENV_GAIN);
  /* RESTORE(agc,CSPI_ENV_AGC); */
  RESTORE(mtvcxoffs,CSPI_ENV_MTVCXOFFS);
  RESTORE(mtncoshft,CSPI_ENV_MTNCOSHFT);

  int rc = cspi_setenvparam(conn.envHandle, &params, change);
  if (rc)
    cerr << "restore_backup() " << CSPIHelper::GetCSPIError(rc) << endl;
  return rc;

}

// ----------------------------------------------------------------------------------------

void ConfigThread::wakeup() {
  // Wake up thread
  pthread_cond_signal( &command_cond );
}

void ConfigThread::add_command(string name,CSPI_BITMASK code, double param) {

  command_array.lock();
  COMMAND cmd;
  cmd.command = code;
  cmd.param = param;
  cmd.name = name;
  commands.push_back(cmd);
  command_array.unlock();

  // Inform that a new command has been added
  wakeup();

}

int ConfigThread::get_command_count() {

  omni_mutex_lock l(command_array);
  return commands.size();

}


#define PTRCMD(_rc,_cmd,_param) \
if (_rc) \
  cerr << cmd.name << " failed:" << CSPIHelper::GetCSPIError(_rc) << endl; \
else \
  cout << _cmd << "(" << _param << ") ok !" << endl

void ConfigThread::execute_command() {


  string cmdName;

  COMMAND cmd;
  command_array.lock();
  cmd = commands[0];
  commands.erase(commands.begin());
  command_array.unlock();

  switch (cmd.command) {

    case CSPI_ENV_ILKSTATUS:
      params.ilk_status = 0;
      break;

    case CSPI_ENV_XOFFSET:
      params.Xoffset = (int) cmd.param;
      break;

    case CSPI_ENV_YOFFSET:
      params.Yoffset = (int) cmd.param;
      break;

    case CSPI_ENV_QOFFSET:
      params.Qoffset = (int) cmd.param;
      break;

    case CSPI_ENV_TRIGDELAY:
      params.trig_delay = (int) cmd.param;
      break;

    case CSPI_ENV_PMOFFSET:
      params.PMoffset = (int) cmd.param;
      break;

    case CSPI_ENV_PMDEC:
      params.PMdec = (int) cmd.param;
      break;

    case CSPI_ENV_PMMODE:
      params.pm.mode = (int) cmd.param;
      cmd.command = CSPI_ENV_PM;
      break;

    case CSPI_ENV_SWITCH:
      params.switches = (int) cmd.param;
      break;

    case CSPI_ENV_SWDELAY:
      params.switching_delay = (int) cmd.param;
      break;

    case CSPI_ENV_EXTSWITCH:
      params.external_switching = (int) cmd.param;
      break;

    case CSPI_ENV_DSC:
      params.dsc = (int) cmd.param;
      break;

    case CSPI_ENV_GAIN:
      params.gain = (int) cmd.param;
      break;

    case CSPI_ENV_AGC:
      params.agc = (int) cmd.param;
      break;

    case CSPI_ENV_MTVCXOFFS:
      params.mtvcxoffs = cmd.param;
      break;

    case CSPI_ENV_MTNCOSHFT:
      params.mtncoshft = cmd.param;
      break;

    case CSPI_ENV_DDC_MAFLENGTH:
      params.ddc_maflength = cmd.param;
      break;

    case CSPI_ENV_DDC_MAFDELAY:
      params.ddc_mafdelay = cmd.param;
      break;

    case CSPI_ENV_TRIGMODE:
    {

      CSPI_BITMASK mask = CSPI_TIME_MT | CSPI_TIME_ST;
      CSPI_SETTIMESTAMP ts;
      ts.mt = (uint64_t) ds->attr_MachineTime_read[0];
      ts.st.tv_sec = (uint32_t) ds->attr_SystemTime_read[0];
      ts.st.tv_nsec = 0;
      ts.phase = 100;
      int rc = cspi_settime(conn.envHandle, &ts, mask);
      PTRCMD(rc, "cspi_settime", "0");
      params.trig_mode = cmd.param;

    }
    break;

    case CSPI_ENV_STARTCOMCONTROLLER:
    {
      uint32_t reg = 0;
      int rc;

      rc = cspi_setenvparam_fa(conn.envHandle, CSPI_ENVPARAMS_DCC_EXT_TRIGGER, &reg, sizeof(reg), 1);
      PTRCMD(rc,"CSPI_ENVPARAMS_DCC_EXT_TRIGGER","0");

      rc = cspi_setenvparam_fa(conn.envHandle, CSPI_ENVPARAMS_DCC_INTERFACE, &reg, sizeof(reg), 1);
      PTRCMD(rc,"CSPI_ENVPARAMS_DCC_INTERFACE","0");

      rc = cspi_setenvparam_fa(conn.envHandle, CSPI_ENVPARAMS_DCC_CONTROL, &reg, sizeof(reg), 1);
      PTRCMD(rc,"CSPI_ENVPARAMS_DCC_CONTROL","0");

      rc = cspi_setenvparam_fa(conn.envHandle, CSPI_ENVPARAMS_DCC_EXT_TRIGGER, &reg, sizeof(reg), 1);
      PTRCMD(rc,"CSPI_ENVPARAMS_DCC_EXT_TRIGGER","0");

      reg = 8;
      rc = cspi_setenvparam_fa(conn.envHandle, CSPI_ENVPARAMS_DCC_CONTROL, &reg, sizeof(reg), 1);
      PTRCMD(rc,"CSPI_ENVPARAMS_DCC_CONTROL","8");

      reg = 1;
      rc = cspi_setenvparam_fa(conn.envHandle, CSPI_ENVPARAMS_DCC_EXT_TRIGGER, &reg, sizeof(reg), 1);
      PTRCMD(rc,"CSPI_ENVPARAMS_DCC_EXT_TRIGGER","1");

      return;
    }
    break;



    default:
      cerr << "Unexpected command " << cmd.name << " #" << cmd.command << endl;
      return;

  }

  int rc = cspi_setenvparam(conn.envHandle, &params, cmd.command);

  if (rc)
    cerr << cmd.name << " failed:" << CSPIHelper::GetCSPIError(rc) << endl;
  else
    cout << cmd.name << "(" << cmd.param << ") ok !" << endl;

}

int ConfigThread::dump_env_parameters() {


  int rc = cspi_getenvparam(conn.envHandle,&params,allMask);
  // Copy param from MT controller
  params.mtvcxoffs = params.pll_status.mt_stat.vcxo_offset;
  params.mtncoshft = params.pll_status.mt_stat.nco_shift;

  // Read FA infos
  rc = ::cspi_getenvparam_fa(conn.envHandle,0xC00,FAIParams,4,34);


#if 0
  if(!rc) {
  cout << "- Feature.customer..............0x" << hex << params.feature.customer << dec << endl;
  cout << "- Feature.itech.................0x" << hex << params.feature.itech << dec << endl;
  cout << "- Trigger mode.................." << params.trig_mode << endl;
  cout << "- Kx............................" << params.Kx << endl;
  cout << "- Kz............................" << params.Ky << endl;
  cout << "- Xoffset......................." << params.Xoffset << endl;
  cout << "- Zoffset......................." << params.Yoffset << endl;
  cout << "- Qoffset......................." << params.Qoffset << endl;
  cout << "- Switches......................" << params.switches << endl;
  cout << "- Gain.........................." << params.gain << endl;
  cout << "- AGC..........................." << params.agc << endl;
  cout << "- DSC..........................." << params.dsc << endl;
  cout << "- Interlocks.Mode..............." << params.ilk.mode << endl;
  cout << "- Interlocks.Xlow..............." << params.ilk.Xlow << endl;
  cout << "- Interlocks.Xhigh.............." << params.ilk.Xhigh << endl;
  cout << "- Interlocks.Zlow..............." << params.ilk.Ylow << endl;
  cout << "- Interlocks.Zhigh.............." << params.ilk.Yhigh << endl;
  cout << "- Interlocks.UnderflowLimit......" << params.ilk.underflow_limit << endl;
  cout << "- Interlocks.OverflowLimit......" << params.ilk.overflow_limit << endl;
  cout << "- Interlocks.OverflowDuration..." << params.ilk.overflow_dur << endl;
  cout << "- Interlocks.GainLimit.........." << params.ilk.gain_limit << endl;
  cout << "- External Switching ..........." << params.external_switching << endl;
  cout << "- Switching Delay..............." << params.switching_delay << endl;
  cout << "- Tune Compensation ............" << params.pll_status.mt_stat.nco_shift << endl;
  cout << "- Tune Offset .................." << params.pll_status.mt_stat.vcxo_offset << endl;
  cout << "- External Trigger Delay........" << params.trig_delay << endl;
  cout << "- Post Mortem Offset............" << params.PMoffset << endl;
  cout << "- Post Mortem Decimation........" << params.PMdec << endl;
  cout << "- Lifetime window .............." << params.lifetime_window << endl;
  cout << "- Has MAF support..............." << LIBERA_IS_MAF(params.feature.itech) << endl;
  if (LIBERA_IS_MAF(params.feature.itech))
  {
    cout << "- MAF Length...................." << params.ddc_maflength << endl;
    cout << "- MAF Delay....................." << params.ddc_mafdelay << endl;
  }
  cout << "- DD Stat Decimation ............." << params.dd_stats_dec << endl;
  cout << "- Partial BeamLoss Coeff ........." << params.delta_sum_coeff << endl;
  cout << "- Partial BeamLoss Control ......." << params.delta_sum_control << endl;
  cout << "- Partial BeamLoss Status ........" << params.delta_sum_status << endl;
  }
#endif

  return rc;

}


}
