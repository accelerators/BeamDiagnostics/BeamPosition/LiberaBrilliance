//+=============================================================================
//
// file :         SAThread.h
//
// description :  Include for the ConnectionThread class.
//                This class is used for reading SA stream
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLIANCE_SATHREAD_H
#define LIBERABRILLIANCE_SATHREAD_H

#include <LiberaBrilliance.h>

namespace LiberaBrilliance_ns {

class SAThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    SAThread(LiberaBrilliance *, PosCalculation *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

    void setHistoryLength(int length);
    int  getHistoryLength();
    void setAveraging(bool avg);
    bool isAveraging();
    void setAvgLength(int length);
    int  getAvgLength();
    void setStatLength(int length);
    int  getStatLength();

    double GetVa();
    double GetVb();
    double GetVc();
    double GetVd();
    double GetSum();
    double GetH();
    double GetV();
    double GetHStdDev();
    double GetVStdDev();
    double GetQ();

    void getVaHistory(double *hist,int *length);
    void getVbHistory(double *hist,int *length);
    void getVcHistory(double *hist,int *length);
    void getVdHistory(double *hist,int *length);
    void getSumHistory(double *hist,int *length);
    void getHHistory(double *hist, int *length);
    void getVHistory(double *hist, int *length);
    void getQHistory(double *hist,int *length);

    void wakeup();

private:

    void computeAveraging();
    void computeStats();
    void ResetConnection();

    LiberaBrilliance *ds;
    omni_mutex &mutex;
    CSPI_CONN conn;
    PosCalculation *PC;
    vector<BP> saValues;
    int historyLength;
    int avgLength;
    int statLength;
    bool average;
    double HStdDev;
    double VStdDev;

    BP currentPos;

}; // class SAThread

} // namespace LiberaBrilliance_ns

#endif //LIBERABRILLIANCE_SATHREAD_H
