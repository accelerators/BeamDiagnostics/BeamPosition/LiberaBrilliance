//+=============================================================================
//
// file :         PositionCalculation.h
//
// description :  This class is used for computing beam position
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaBrilliance_ns
{
class PosCalculation;
}

#include "PosCalculation.h"
#include "LiberaBrilliance.h"

namespace LiberaBrilliance_ns {



// --------------------------------------------------------------------------------------

#define SEARCH_MAX(poly)                        \
for(int i=0;i<(int)poly.size();i++) {           \
  if(poly[i].nX>maxXPow) maxXPow = poly[i].nX;  \
  if(poly[i].nY>maxYPow) maxYPow = poly[i].nY;  \
}

void PosCalculation::SetPolynomial(vector<POLY>& hPoly,vector<POLY>& vPoly,vector<POLY>& qPoly,vector<POLY>& sPoly) {

  this->hPoly = hPoly;
  this->vPoly = vPoly;
  this->qPoly = qPoly;
  this->sPoly = sPoly;
  maxXPow = 0;
  maxYPow = 0;
  SEARCH_MAX(hPoly);
  SEARCH_MAX(vPoly);
  SEARCH_MAX(qPoly);
  SEARCH_MAX(sPoly);

}


// --------------------------------------------------------------------------------------

void PosCalculation::SetAlgorithm(int algo) {

  calculationAlgorithm = algo;

}

// --------------------------------------------------------------------------------------

void PosCalculation::SetTiltAngle(double angle) {

  tiltAngle = angle;
  r11 = cos(angle);r12 = -sin(angle);
  r21 = sin(angle);r22 = cos(angle);

}

// --------------------------------------------------------------------------------------

int  PosCalculation::GetAlgorithm() {

  return calculationAlgorithm;

}

// --------------------------------------------------------------------------------------

PosCalculation::PosCalculation(double st) {

  sumSAThreshold = st;
  kA=1.0;
  kB=1.0;
  kC=1.0;
  kD=1.0;
  // Rotation matrix
  tiltAngle = 0.0;
  r11 = 1.0; r12 = 0.0;
  r21 = 0.0; r22 = 1.0;
  KH = 0;
  KV = 0;
  OH = 0;
  OV = 0;
  OQ = 0;

}

// --------------------------------------------------------------------------------------

void PosCalculation::ComputePosition(CSPI_SA_ATOM &sa, BP &p) {

  p.va = sa.Va;
  p.vb = sa.Vb;
  p.vc = sa.Vc;
  p.vd = sa.Vd;
  ComputePosition(p);

}

// --------------------------------------------------------------------------------------

void PosCalculation::ComputePosition(CSPI_DD_RAWATOM &dd, BP &p, bool loadIQ) {

  if( loadIQ ) {
    p.ia = dd.sinVa;
    p.qa = dd.cosVa;
    p.ib = dd.sinVb;
    p.qb = dd.cosVb;
    p.ic = dd.sinVc;
    p.qc = dd.cosVc;
    p.id = dd.sinVd;
    p.qd = dd.cosVd;
  }
  ComputePosition(p);

}

// --------------------------------------------------------------------------------------
#define EVAL_POLY(poly)                             \
sum = 0.0;                                          \
for (size_t j(0); j < poly.size(); j++)             \
sum += px[poly[j].nX] * py[poly[j].nY] * poly[j].c;

void PosCalculation::ComputePosition(BP &p) {

  // Inputs are in nm
  double KQ = (KH + KV) / 2.0;

  p.va *= kA;
  p.vb *= kB;
  p.vc *= kC;
  p.vd *= kD;

  double a =  p.va;
  double b =  p.vb;
  double c =  p.vc;
  double d =  p.vd;

  p.sum = a + b + c + d;

  if (p.sum < sumSAThreshold) {
    p.h = NAN;
    p.v = NAN;
    p.q = NAN;
    return;
  }

  switch( calculationAlgorithm ) {

    // Delta Over Sigma calculation
    case DOS_CALC: {

      // Pos in m
      p.h = (KH * (((a + d) - (b + c)) / p.sum) - OH)*1e-9;
      p.v = (KV * (((a + b) - (c + d)) / p.sum) - OV)*1e-9;
      p.q = (KQ * (((a + c) - (b + d)) / p.sum) - OQ)*1e-9;

    }
    break;


      // Compute position according to polynomial transform
    case POLYNOMIAL_CALC: {

      double iS = 1.0 / (a + b + c + d);
      double X = (a - b - c + d) * iS;
      double Y = (a + b - c - d) * iS;
      double q = (a - b + c - d) * iS;
      double sum;

      double powx = 1.0;
      double powy = 1.0;
      for (int j(0); j <= maxXPow; j++) {
        px[j] = powx;
        powx *= X;
      }
      for (int j(0); j <= maxYPow; j++) {
        py[j] = powy;
        powy *= Y;
      }

      EVAL_POLY(hPoly);
      p.h = (1e6*sum - OH)*1e-9;

      EVAL_POLY(vPoly);
      p.v = (1e6*sum - OV)*1e-9;

      // For S and Q, we need coordinates in mm
      X = p.h * 1e3;
      Y = p.v * 1e3;
      powx = 1.0;
      powy = 1.0;
      for (int j(0); j <= maxXPow; j++) {
        px[j] = powx;
        powx *= X;
      }
      for (int j(0); j <= maxYPow; j++) {
        py[j] = powy;
        powy *= Y;
      }

      EVAL_POLY(qPoly);
      p.q = (KQ*(q-sum) - OQ)*1e-9;

      EVAL_POLY(sPoly);
      p.sum = p.sum / sum;

    }
    break;

  }

  // Tilt rotation
  double rh = r11 * p.h + r12 * p.v;
  double rv = r21 * p.h + r22 * p.v;
  p.h = rh;
  p.v = rv;

}

} // LiberaBrilliance_ns
