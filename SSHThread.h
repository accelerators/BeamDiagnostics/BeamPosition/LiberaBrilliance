//+=============================================================================
//
// file :         SSHThread.h
//
// description :  Include for the SSHThread class.
//                This class is used for executing remotly command on the libera
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLIANCE_SSHTHREAD_H
#define LIBERABRILLIANCE_SSHTHREAD_H

#include <LiberaBrilliance.h>
#include <libssh/libssh.h>
#include <libssh/callbacks.h>

namespace LiberaBrilliance_ns {

class SSHThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    SSHThread(LiberaBrilliance *, omni_mutex &, int command);
    void run(void *);
    string getOutput();

    void outputString(std::string str);

private:

    std::vector<string> output;

    void readLine(char *buffer,int buffLength);
    void flush();
    void disconnect();
    bool ssh_execute(string command);
    void change_MAF(int firmware);
    void restart_daemon();

    LiberaBrilliance *ds;
    omni_mutex &mutex;

    int command;

    int  innerBufferSize;
    char innerBuffer[16384];

    // ssh stuff
    ssh_session session;
    ssh_channel channel;


public:


}; // class SSHThread

} // namespace LiberaBrilliance_ns

#endif //LIBERABRILLIANCE_SSHTHREAD_H
