//+=============================================================================
//
// file :         DDThread.h
//
// description :  Include for the DDThread class.
//                This class is used for reading DD data (TBT)
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLIANCE_DDTHREAD_H
#define LIBERABRILLIANCE_DDTHREAD_H

#include <LiberaBrilliance.h>

namespace LiberaBrilliance_ns {

class DDThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    DDThread(LiberaBrilliance *,PosCalculation *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

    unsigned int getTriggerCounter();
    void resetTriggerCounter();

private:

    vector<BP> ddValues;
    void computePosition(CSPI_DD_RAWATOM *dd,size_t nb);
    void ResetConnection();
    int ReadData(CSPI_DD_RAWATOM *ddData, size_t size);
    LiberaBrilliance *ds;
    omni_mutex &mutex;
    PosCalculation *PC;
    CSPI_CONN conn;
    unsigned int trigCount;
    void fireDataReady();


public:

    pthread_cond_t trigger_cond;
    pthread_mutex_t trigger_mutex;

    double *getVa(int *length);
    double *getVb(int *length);
    double *getVc(int *length);
    double *getVd(int *length);
    double *getIa(int *length);
    double *getIb(int *length);
    double *getIc(int *length);
    double *getId(int *length);
    double *getQa(int *length);
    double *getQb(int *length);
    double *getQc(int *length);
    double *getQd(int *length);
    double *getSum(int *length);
    double *getQ(int *length);
    double *getH(int *length);
    double *getV(int *length);

    void wakeup();

}; // class DDThread

} // namespace LiberaBrilliance_ns

#endif //LIBERABRILLIANCE_DDTHREAD_H
