//+=============================================================================
//
// file :         CSPIHelper.h
//
// description :  CSPI Helper class
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLANCE_CSPIHELPER_H
#define LIBERABRILLANCE_CSPIHELPER_H

// Define Libera Device Family (Electron Beam Position Processor)
#define EBPP

#include <client-lib.h>
#include <error.h>
#include <ebpp.h>

typedef struct {

    std::string name;
    int flags;
    CSPIHANDLE conHandle;
    CSPI_CONPARAMS_EBPP conParams;
    CSPIHANDLE envHandle;

} CSPI_CONN;

class CSPIHelper {

public:

    /**
     * Init CSPI Helper
     * @param ipAddr IP address of the libera
     * @param port Port number
     * @param multicastIpAddr Multicast addr
     */
    static void Init(std::string ipAddr,int port,std::string multicastIpAddr);

    /**
     * Build a connection to the CSPI.
     * @param conn Connection parameters
     * @param error Error string in case of failure
     * @return true on succes, false otherwise
     */
    static bool CSPIConnect(CSPI_CONN &conn,std::string &error);

    /**
     * Disconnect
     * @param conn Connection handle
     */
    static void CSPIDisconnect(CSPI_CONN &conn);

    static void ClearConn(CSPI_CONN &conn);
    static std::string GetServerError();
    static std::string GetCSPIError(int code);
    static std::string GetDate();
    static time_t GetTicks();

};


#endif //LIBERABRILLIANCE_CSPIHELPER_H
