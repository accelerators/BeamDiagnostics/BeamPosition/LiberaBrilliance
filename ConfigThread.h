//+=============================================================================
//
// file :         ConfigThread.h
//
// description :  Include for the ConfigThread class.
//                This class is used for Brilliance configuration
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLIANCE_CONFIGTHREAD_H
#define LIBERABRILLIANCE_CONFIGTHREAD_H

#include <LiberaBrilliance.h>

namespace LiberaBrilliance_ns {

typedef struct {

    string name;
    CSPI_BITMASK command;
    double param;

} COMMAND;

class ConfigThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    ConfigThread(LiberaBrilliance *, omni_mutex &);
    ConfigThread(LiberaBrilliance *, omni_mutex &,void *);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

    double getKH();
    double getKV();
    double getOffsetH();
    double getOffsetV();
    double getOffsetQ();
    double getHWTemp1();
    double getHWTemp2();
    double getHWTemp3();
    double getFan1Speed();
    double getFan2Speed();
    int getExternalTriggerDelay();
    int getPMMode();
    int getPMOffset();
    int getPMDecFactor();
    int getSwitches();
    int getSwitchingDelay();
    int getExternalSwitching();
    double getAvgSumBetweenTrigger();
    int getDSCMode();
    int getDSCStatus();
    double getDSCCoefA();
    double getDSCCoefB();
    double getDSCCoefC();
    double getDSCCoefD();
    double getGain();
    bool getAGC();
    vector<string> getInterlockConfig();
    bool getItlkH();
    bool getItlkV();
    bool getItlkAtt();
    bool getItlkADCOverflowNoFilter();
    bool getItlkADCOverflowFilter();
    bool getItlkADCUnderflow();
    int getOffsetTune();
    bool getCompensateTune();
    bool getMCPLLStatus();
    bool getSCPLLStatus();
    bool hasMAF();
    bool isInterlockEnabled();
    string getMTStatus();
    int getMAFLength();
    int getMAFDelay();
    int getDCCProcessTime();
    int *getFAIParams();

    void resetItlk();
    void setOffsetH(double offsetH);
    void setOffsetV(double offsetV);
    void setOffsetQ(double offsetQ);
    void setExternalTriggerDelay(int delay);
    void setPMOffset(int offset);
    void setPMDecFactor(int factor);
    void setSwitches(int switches);
    void setSwitchingDelay(int delay);
    void setExternalSwitching(int external);
    void setDSCMode(int mode);
    void setGain(double gain);
    void setAGC(bool agc);
    void setOffsetTune(int tune);
    void setCompensateTune(bool compensate);
    void setPMMode(int mode);
    void setMAFLength(int length);
    void setMAFDelay(int delay);
    void setTrigMode(int trigMode);
    void startComController();

    void wakeup();

    void *getBackup();

private:

    LiberaBrilliance *ds;
    omni_mutex &mutex;
    CSPI_CONN conn;
    CSPI_ENVPARAMS params;
    CSPI_ENVPARAMS params_backup;
    int32_t FAIParams[34];
    void *initBackup;
    bool firstConnect;

    void init();
    int restore_backup();
    int write_default();
    int dump_env_parameters();
    void execute_command();
    int get_command_count();
    void add_command(string name,CSPI_BITMASK code,double param);

    pthread_cond_t command_cond;
    pthread_mutex_t command_mutex;
    omni_mutex command_array;
    vector<COMMAND> commands;


}; // class ConfigThread

} // namespace LiberaBrilliance_ns

#endif //LIBERABRILLIANCE_CONFIGTHREAD_H
