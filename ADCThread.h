//+=============================================================================
//
// file :         ADCThread.h
//
// description :  Include for the ADCThread class.
//                This class is used for reading ADC data
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLIANCE_ADCTHREAD_H
#define LIBERABRILLIANCE_ADCTHREAD_H

#include <LiberaBrilliance.h>

namespace LiberaBrilliance_ns {

class ADCThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    ADCThread(LiberaBrilliance *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

 private:

    void ResetConnection();
    int ReadData(CSPI_ADC_ATOM *adcData, size_t size);
    void copyData(CSPI_ADC_ATOM *dd,size_t nb);

    vector<ADC> adcValues;
    LiberaBrilliance *ds;
    omni_mutex &mutex;
    CSPI_CONN conn;

public:

    pthread_cond_t trigger_cond;
    pthread_mutex_t trigger_mutex;

    int16_t *getVa(int *length);
    int16_t *getVb(int *length);
    int16_t *getVc(int *length);
    int16_t *getVd(int *length);
    void wakeup();

}; // class ADCThread

} // namespace LiberaBrilliance_ns

#endif //LIBERABRILLIANCE_ADCTHREAD_H
