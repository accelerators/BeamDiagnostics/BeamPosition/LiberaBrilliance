//+=============================================================================
//
// file :         DDThread.cpp
//
// description :  Include for the DDThread class.
//                This class is used for reading DD data (TBT)
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaBrilliance_ns
{
class DDThread;
}

#include <DDThread.h>
#include "CSPIHelper.h"

namespace LiberaBrilliance_ns
{

// CSPI Callback function
static int _cspi_callback( CSPI_EVENT *msg )
{
  DDThread *th = (DDThread *)msg->user_data;
  if(th->isConnected)
    th->wakeup();
  return 1;
}

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
DDThread::DDThread(LiberaBrilliance *libera, PosCalculation *pc,omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), PC(pc), ds(libera)
{

  INFO_STREAM << "DDThread: Starting DD Thread." << endl;
  trigger_cond = PTHREAD_COND_INITIALIZER;
  trigger_mutex = PTHREAD_MUTEX_INITIALIZER;

  trigCount = 0;
  threadStatus = "DDThread: connecting";

  start_undetached();
}

// ----------------------------------------------------------------------------------------

void DDThread::ResetConnection() {


  // Initialise connection parameters
  CSPIHelper::ClearConn(conn);
  conn.name = "DD";
  conn.flags = CSPI_CON_MODE | CSPI_CON_EVENTMASK | CSPI_CON_HANDLER | CSPI_CON_USERDATA;
  conn.conParams.mode = CSPI_MODE_DD;
  conn.conParams.event_mask = CSPI_EVENT_TRIGGET;
  conn.conParams.handler = _cspi_callback;
  conn.conParams.user_data = this;

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *DDThread::run_undetached(void *arg) {

  string errorStr;
  int rc;

  cout << "DDThread: entering." << endl;

  exitThread = false;
  isConnected = false;

  while(!exitThread) {

    // Connection

    while (!isConnected && !exitThread) {

      ResetConnection();
      if (!CSPIHelper::CSPIConnect(conn, errorStr)) {
        // Connect failure
        mutex.lock();
        threadStatus = errorStr;
        mutex.unlock();
        cout << errorStr << endl;
        CSPIHelper::CSPIDisconnect(conn);
        sleep(3);
      } else {

        // Set decimation factor
        conn.conParams.dec = (ds->attr_TBT_Dec_Enable_read[0])?64:1;
        rc = cspi_setconparam(conn.conHandle, reinterpret_cast < CSPI_CONPARAMS * >(&conn.conParams), CSPI_CON_DEC);

        if (!rc) {
          isConnected = true;
        } else {
          mutex.lock();
          threadStatus = "DDThread: " + CSPIHelper::GetCSPIError(rc);
          mutex.unlock();
          CSPIHelper::CSPIDisconnect(conn);
          sleep(3);
        }

      }

    }

    cout << "DDThread: connected." << endl;

    // Read data
    mutex.lock();
    threadStatus = "DDThread: waiting";
    mutex.unlock();

    while (isConnected && !exitThread) {

#define NETWORK_EVENT
#ifdef NETWORK_EVENT
      // Wait for data
      struct timeval now;
      struct timespec timeout;
      gettimeofday(&now, 0);

      ds->attr_TBT_IQ_DataReady_read[0] = false;
      ds->attr_TBT_DataReady_read[0] = false;

      // Wait for ever
      timeout.tv_sec = INT32_MAX;
      timeout.tv_nsec = now.tv_usec * 1000;

      rc = pthread_cond_timedwait(&trigger_cond,
                                  &trigger_mutex,
                                  &timeout);

#endif

      if (!rc) {

        if (!exitThread) {

          // Read DD data
          //unsigned long long offset = (unsigned long long)ds->attr_TBT_Offset_read[0];
          unsigned long long offset = 0;
          rc = ::cspi_seek(conn.conHandle, &offset, CSPI_SEEK_TR);

          size_t size = (size_t)ds->attr_TBT_BufSize_read[0];
          CSPI_DD_RAWATOM *ddData = (CSPI_DD_RAWATOM *)malloc(size*sizeof(CSPI_DD_RAWATOM));
          rc = ReadData(ddData,size);

          if(rc) {

            cerr << "DDThread::cspi_read_ex() failed " << CSPIHelper::GetCSPIError(rc) << " " << CSPIHelper::GetDate()
                 << endl;

            // We may got a disconnection from the server
            // Retry once
            isConnected = false;
            CSPIHelper::CSPIDisconnect(conn);
            ResetConnection();
            if (CSPIHelper::CSPIConnect(conn, errorStr)) {
              // Restore decimated setting
              conn.conParams.dec = (ds->attr_TBT_Dec_Enable_read[0])?64:1;
              rc = cspi_setconparam(conn.conHandle, reinterpret_cast < CSPI_CONPARAMS * >(&conn.conParams), CSPI_CON_DEC);
              isConnected = true;
              rc = ReadData(ddData,size);
            }

            if( rc ) {

              cerr << "DDThread::cspi_read_ex() failed " << CSPIHelper::GetCSPIError(rc) << " " << CSPIHelper::GetDate()
                   << endl;
              free(ddData);
              // Communication error, go to reconnection
              mutex.lock();
              threadStatus = "DDThread: " + CSPIHelper::GetCSPIError(rc);
              ddValues.clear();
              mutex.unlock();
              isConnected = false;
              CSPIHelper::CSPIDisconnect(conn);

            } else {

              computePosition(ddData,size);
              free(ddData);
              fireDataReady();

            }

          } else {

            computePosition(ddData,size);
            free(ddData);
            fireDataReady();

          }


        }

      } else {

        cerr << "DDThread: pthread_cond_timedwait failed: " << rc << endl;

      }

      trigCount++;

    }

  }

  cout << "DDThread: disconnecting." << endl;

  time_t t0 = CSPIHelper::GetTicks();
  isConnected = false;
  CSPIHelper::CSPIDisconnect(conn);
  time_t t1 = CSPIHelper::GetTicks();
  ds->attr_Time_TBTClose_read[0] = (t1-t0);

  cout << "DDThread: exiting." << endl;
  return NULL;

}

int DDThread::ReadData(CSPI_DD_RAWATOM *ddData, size_t size) {

  int rc;

  if(size==0)
    return 0;

  size_t nbRead=0;
  rc = ::cspi_read_ex(conn.conHandle, ddData, size, &nbRead, 0);

  //cout << "DDThread: Got " << nbRead << endl;

  //- <cspi_read_ex> special error case: err:-5 and errno:61: no data available
  //----------------------------------------------------------------------------
  //- this might occur when usign a huge DD buffer size. in this case reading
  //- DD data on Libera side is VERY slow and DD data might be overwritten in
  //- the circular DD data buffer on Libera side (or something similar). let's
  //- ignore this error...

  if (rc == CSPI_W_INCOMPLETE) {

    // From time to time, few samples can be missed at the end of the buffer
    // Fill with 0

    size_t missing_samples = size - nbRead;
    size_t start_fill = size - missing_samples;

    //cout << "DDThread: Adjusting nb of samples, adding " << missing_samples << " samples\n";

    for (size_t a = start_fill; a < size; a++) {
      ::memset(&(ddData[a]), 0, sizeof(CSPI_DD_RAWATOM));
    }
    nbRead = size;
    rc = 0;

  }

  return rc;

}

void DDThread::fireDataReady() {

  ds->attr_TBT_IQ_DataReady_read[0] = true;
  ds->attr_TBT_DataReady_read[0] = true;
  try {
    if (ds->attr_TBT_IQ_Enable_read[0])
      ds->push_data_ready_event("TBT_IQ_DataReady");
    if (ds->attr_TBT_Enable_read[0])
      ds->push_data_ready_event("TBT_DataReady");
  } catch (Tango::DevFailed &e) {
    cout << "DDThread: Warning, push_data_ready_event failed: " << e.errors[0].desc << endl;
  }

}

// ----------------------------------------------------------------------------------------

#define GET_VALUES(field)                      \
  double *ret = NULL;                          \
  omni_mutex_lock l(mutex);                    \
  *length = ddValues.size();                   \
  if(ddValues.size()>0) {                      \
    ret = new double[ddValues.size()];         \
    for(int i=0;i<(int)ddValues.size();i++)    \
      ret[i] = ddValues[i].field;              \
  }                                            \
  return ret;

double *DDThread::getVa(int *length) {
  GET_VALUES(va);
}
double *DDThread::getVb(int *length) {
  GET_VALUES(vb);
}
double *DDThread::getVc(int *length) {
  GET_VALUES(vc);
}
double *DDThread::getVd(int *length) {
  GET_VALUES(vd);
}
double *DDThread::getSum(int *length) {
  GET_VALUES(sum);
}
double *DDThread::getQ(int *length) {
  GET_VALUES(q);
}
double *DDThread::getH(int *length) {
  GET_VALUES(h);
}
double *DDThread::getV(int *length) {
  GET_VALUES(v);
}
double *DDThread::getIa(int *length) {
  GET_VALUES(ia);
}
double *DDThread::getIb(int *length) {
  GET_VALUES(ib);
}
double *DDThread::getIc(int *length) {
  GET_VALUES(ic);
}
double *DDThread::getId(int *length) {
  GET_VALUES(id);
}
double *DDThread::getQa(int *length) {
  GET_VALUES(qa);
}
double *DDThread::getQb(int *length) {
  GET_VALUES(qb);
}
double *DDThread::getQc(int *length) {
  GET_VALUES(qc);
}
double *DDThread::getQd(int *length) {
  GET_VALUES(qd);
}

// ----------------------------------------------------------------------------------------

const double ASfilter[] = { 0.0022, -0.0133, 0.0664, -0.3186, 1.5274, -0.3186, 0.0664, -0.0133, 0.0022 };


void DDThread::computePosition(CSPI_DD_RAWATOM *dd,size_t nb) {

  mutex.lock();

  BP p;
  p.ia = NAN;
  p.qa = NAN;
  p.ib = NAN;
  p.qb = NAN;
  p.ic = NAN;
  p.qc = NAN;
  p.id = NAN;
  p.qd = NAN;

  if( ds->attr_TBT_AntiSmearing_read[0] ) {

    // Crop corrupted samples
    int kernelSize = sizeof(ASfilter)/sizeof(double);
    int resultSize = nb - (kernelSize-1);
    ddValues.clear();

    if(resultSize>0) {

      ddValues.reserve(resultSize);
      double *inVa = (double *)malloc(nb*sizeof(double));
      double *inVb = (double *)malloc(nb*sizeof(double));
      double *inVc = (double *)malloc(nb*sizeof(double));
      double *inVd = (double *)malloc(nb*sizeof(double));
      for(int i=0;i<(int)nb;i++) {
        inVa[i] = CORDIC(dd[i].sinVa, dd[i].cosVa);
        inVb[i] = CORDIC(dd[i].sinVb, dd[i].cosVb);
        inVc[i] = CORDIC(dd[i].sinVc, dd[i].cosVc);
        inVd[i] = CORDIC(dd[i].sinVd, dd[i].cosVd);
      }
      for(int i=0;i<resultSize;i++) {
        double suma = 0.0;
        double sumb = 0.0;
        double sumc = 0.0;
        double sumd = 0.0;
        for(int k=0;k<kernelSize;k++) {
          suma += inVa[i + k] * ASfilter[k];
          sumb += inVb[i + k] * ASfilter[k];
          sumc += inVc[i + k] * ASfilter[k];
          sumd += inVd[i + k] * ASfilter[k];
        }
        p.va = suma;
        p.vb = sumb;
        p.vc = sumc;
        p.vd = sumd;
        PC->ComputePosition(dd[i+kernelSize/2], p, ds->attr_TBT_IQ_Enable_read[0]);
        ddValues.push_back(p);
      }
      free(inVa);
      free(inVb);
      free(inVc);
      free(inVd);

    }

  } else {

    ddValues.clear();
    ddValues.reserve(nb);
    for (int i = 0; i < (int) nb; i++) {

      p.va = CORDIC(dd[i].sinVa, dd[i].cosVa);
      p.vb = CORDIC(dd[i].sinVb, dd[i].cosVb);
      p.vc = CORDIC(dd[i].sinVc, dd[i].cosVc);
      p.vd = CORDIC(dd[i].sinVd, dd[i].cosVd);
      PC->ComputePosition(dd[i], p, ds->attr_TBT_IQ_Enable_read[0]);
      ddValues.push_back(p);

    }

  }

  mutex.unlock();

}

// ----------------------------------------------------------------------------------------

unsigned int DDThread::getTriggerCounter() {
  return trigCount;
}

void DDThread::resetTriggerCounter() {
  trigCount = 0;
}

// ----------------------------------------------------------------------------------------

void DDThread::wakeup() {
  // Wake up thread
  //cout << "DDThread: wake up." << endl;
  pthread_cond_signal( &trigger_cond );
}

} // namespace LiberaBrilliance_ns
