//+=============================================================================
//
// file :         MaxADCThread.h
//
// description :  Include for the MaxADCThread class.
//                This class is used for Brilliance configuration
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLANCE_MAXADCTHREAD_H
#define LIBERABRILLANCE_MAXADCTHREAD_H

#include <LiberaBrilliance.h>

namespace LiberaBrilliance_ns {

class MaxADCThread : public omni_thread, public Tango::LogAdapter {

public:

  // Constructor
  MaxADCThread(LiberaBrilliance *, omni_mutex &);
  void *run_undetached(void *);
  bool exitThread;
  string threadStatus;
  bool isConnected;

  int getMaxADC();
  void wakeup();

private:

  void init();
  void push_change_event(int value);

  LiberaBrilliance *ds;
  omni_mutex &mutex;
  CSPI_CONN conn;
  CSPI_ENVPARAMS params;

  pthread_cond_t trigger_cond;
  pthread_mutex_t trigger_mutex;


}; // class MaxADCThread

} // namespace LiberaBrilliance_ns


#endif //LIBERABRILLANCE_MAXADCTHREAD_H
