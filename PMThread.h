//+=============================================================================
//
// file :         PMThread.h
//
// description :  Include for the PMThread class.
//                This class is used for reading PM data (Post mortem)
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef LIBERABRILLIANCE_PMTHREAD_H
#define LIBERABRILLIANCE_PMTHREAD_H

#include <LiberaBrilliance.h>

namespace LiberaBrilliance_ns {

class PMThread : public omni_thread, public Tango::LogAdapter {

public:

    // Constructor
    PMThread(LiberaBrilliance *,PosCalculation *, omni_mutex &);
    void *run_undetached(void *);
    bool exitThread;
    string threadStatus;
    bool isConnected;

    unsigned int getTriggerCounter();

private:

    void ResetConnection();
    int ReadData(CSPI_DD_RAWATOM *ddData, size_t size);
    vector<BP> ddValues;
    void computePosition(CSPI_DD_RAWATOM *dd,size_t nb);
    LiberaBrilliance *ds;
    omni_mutex &mutex;
    PosCalculation *PC;
    CSPI_CONN conn;
    unsigned int trigCount;

public:

    pthread_cond_t trigger_cond;
    pthread_mutex_t trigger_mutex;

    double *getVa(int *length);
    double *getVb(int *length);
    double *getVc(int *length);
    double *getVd(int *length);
    double *getIa(int *length);
    double *getIb(int *length);
    double *getIc(int *length);
    double *getId(int *length);
    double *getQa(int *length);
    double *getQb(int *length);
    double *getQc(int *length);
    double *getQd(int *length);
    double *getSum(int *length);
    double *getQ(int *length);
    double *getH(int *length);
    double *getV(int *length);
    void wakeup();

}; // class PMThread

} // namespace LiberaBrilliance_ns

#endif //LIBERABRILLIANCE_PMTHREAD_H
