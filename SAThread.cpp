//+=============================================================================
//
// file :         SAThread.cpp
//
// description :  This class is used for reading SAStream
//
// project :      LiberaBrilliance TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace LiberaBrilliance_ns
{
class SAThread;
}

#include <SAThread.h>
#include "PosCalculation.h"
#include "LiberaBrilliance.h"

#define SQUARE(x) ((x)*(x))

namespace LiberaBrilliance_ns
{

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
SAThread::SAThread(LiberaBrilliance *libera, PosCalculation *pc, omni_mutex &m):
        Tango::LogAdapter(libera), mutex(m), PC(pc), ds(libera)
{
  INFO_STREAM << "SAThread: Starting SA Thread." << endl;

  historyLength = 1800;
  average = false;
  avgLength  = 20;
  statLength = 0;
  currentPos.va = NAN;
  currentPos.vb = NAN;
  currentPos.vc = NAN;
  currentPos.vd = NAN;
  currentPos.sum = NAN;
  currentPos.h = NAN;
  currentPos.v = NAN;
  currentPos.q = NAN;
  HStdDev = NAN;
  VStdDev = NAN;
  threadStatus = "SAThread: connecting";
  start_undetached();
}

// ----------------------------------------------------------------------------------------

void SAThread::ResetConnection() {

  // Initialise connection parameters
  CSPIHelper::ClearConn(conn);
  conn.name = "SAThread";
  conn.flags = CSPI_CON_MODE | CSPI_CON_SANONBLOCK;
  conn.conParams.mode = CSPI_MODE_SA;
  conn.conParams.nonblock = true;

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *SAThread::run_undetached(void *arg) {

  string errorStr;

  exitThread = false;
  isConnected = false;

  cout << "SAThread: entering." << endl;

  while(!exitThread) {

    // Connection

    while(!isConnected && !exitThread) {

      ResetConnection();
      if( !CSPIHelper::CSPIConnect(conn,errorStr) ) {
        // Connect failure
        mutex.lock();
        threadStatus = errorStr;
        mutex.unlock();
        cout << errorStr << endl;
        CSPIHelper::CSPIDisconnect(conn);
        sleep(3);
      } else {
        isConnected = true;
      }

    }

    // Read data
    mutex.lock();
    threadStatus = "SAThread: acquiring";
    mutex.unlock();

    cout << "SAThread: connected." << endl;

    while(isConnected && !exitThread) {

      bool available = true;
      while(available && !exitThread) {

        CSPI_SA_ATOM sa_atom;
        int rc = ::cspi_get(conn.conHandle, &sa_atom);

        if( rc==0 ) {

          // One more sample
          BP p;
          PC->ComputePosition(sa_atom,p);

          // Uupdate buffers and values
          mutex.lock();

          saValues.push_back(p);
          while((int)saValues.size()>historyLength)
            saValues.erase(saValues.begin());

          currentPos = p;

          computeAveraging();
          computeStats();

          mutex.unlock();

          usleep(1000);

        } else if(rc == CSPI_E_SYSTEM && errno == EWOULDBLOCK) {

          // No more sample available
          int toSleep = 500;
          while(toSleep>0 && !exitThread) {
            usleep(100000);
            toSleep -= 100;
          }
          available = false;

        } else {

          cerr << "SAThread::cspi_get() failed " << CSPIHelper::GetCSPIError(rc) << " " << CSPIHelper::GetDate() << endl;

          // Communication error, go to reconnection
          mutex.lock();
          threadStatus = "SAThread: " + CSPIHelper::GetCSPIError(rc);
          mutex.unlock();
          CSPIHelper::CSPIDisconnect(conn);
          isConnected = false;
          available = false;

        }

      }

    }

  }

  cout << "SAThread: disconnecting." << endl;

  CSPIHelper::CSPIDisconnect(conn);

  cout << "SAThread: exiting." << endl;

  return NULL;

}

// ----------------------------------------------------------------------------------------

void SAThread::computeAveraging() {

  if(average) {

    int hSize = (int)saValues.size();
    int start = hSize - avgLength;
    int length = avgLength;
    if(start<0)
      length += start;

    double sumh = 0;
    double sumv = 0;
    double sums = 0;
    for(int i=0;i<length;i++) {
      sumh += saValues[hSize-(i+1)].h;
      sumv += saValues[hSize-(i+1)].v;
      sums += saValues[hSize-(i+1)].sum;
    }
    currentPos.h = sumh / (double)length;
    currentPos.v = sumv / (double)length;
    currentPos.sum = sums / (double)length;

  }

}

void SAThread::computeStats() {

  if(statLength>0) {

    int hSize = (int)saValues.size();
    int start = hSize - statLength;
    int length = statLength;
    if(start<0)
      length += start;

    double sumx = 0;
    double sumz = 0;
    double sumx2 = 0;
    double sumz2 = 0;
    for(int i=0;i<length;i++) {
      sumx += saValues[hSize-(i+1)].h;
      sumz += saValues[hSize-(i+1)].v;
      sumx2 += SQUARE(saValues[hSize-(i+1)].h);
      sumz2 += SQUARE(saValues[hSize-(i+1)].v);
    }

    double avgx = sumx / (double)length;
    double avgz = sumz / (double)length;
    double avgx2 = sumx2 / (double)length;
    double avgz2 = sumz2 / (double)length;

    HStdDev = sqrt( avgx2 - SQUARE(avgx) );
    VStdDev = sqrt( avgz2 - SQUARE(avgz) );

  } else {

    HStdDev = NAN;
    VStdDev = NAN;

  }

}

// ----------------------------------------------------------------------------------------

void SAThread::wakeup() {

}

// ----------------------------------------------------------------------------------------
// Scalar values
// ----------------------------------------------------------------------------------------
#define GET_VAL(v)        \
omni_mutex_lock l(mutex); \
if(!isConnected)          \
  return NAN;             \
else                      \
  return v;

double SAThread::GetH() {

  GET_VAL(currentPos.h);

}

double SAThread::GetV() {

  GET_VAL(currentPos.v);

}

double SAThread::GetQ() {

  GET_VAL(currentPos.q);

}

double SAThread::GetVa() {

  GET_VAL(currentPos.va);

}

double SAThread::GetVb() {

  GET_VAL(currentPos.vb);

}

double SAThread::GetVc() {

  GET_VAL(currentPos.vc);

}

double SAThread::GetVd() {

  GET_VAL(currentPos.vd);

}

double SAThread::GetSum() {

  GET_VAL(currentPos.sum);

}

double SAThread::GetHStdDev() {

  GET_VAL(HStdDev);

}

double SAThread::GetVStdDev() {

  GET_VAL(VStdDev);

}

// ----------------------------------------------------------------------------------------
// Config values
// ----------------------------------------------------------------------------------------

void SAThread::setHistoryLength(int length) {

  omni_mutex_lock l(mutex);
  historyLength = length;

}

int  SAThread::getHistoryLength() {

  return historyLength;

}

void SAThread::setStatLength(int length) {
  omni_mutex_lock l(mutex);
  statLength = length;
}

int SAThread::getStatLength() {
  return statLength;
}

void SAThread::setAveraging(bool avg) {

  average = avg;

}

bool SAThread::isAveraging() {

  return average;

}

void SAThread::setAvgLength(int length) {

  omni_mutex_lock l(mutex);
  avgLength = length;

}

int SAThread::getAvgLength() {

  return avgLength;

}

// ----------------------------------------------------------------------------------------
// History values
// ----------------------------------------------------------------------------------------

#define GET_HIST(field)                   \
  omni_mutex_lock l(mutex);               \
  for(int i=0;i<(int)saValues.size();i++) \
    hist[i]=saValues[i].field;            \
  *length = saValues.size();

void SAThread::getVaHistory(double *hist,int *length) {
  GET_HIST(va);
}

void SAThread::getVbHistory(double *hist,int *length) {
  GET_HIST(vb);
}

void SAThread::getVcHistory(double *hist,int *length) {
  GET_HIST(vc);
}

void SAThread::getVdHistory(double *hist,int *length) {
  GET_HIST(vd);
}

void SAThread::getSumHistory(double *hist,int *length) {
  GET_HIST(sum);
}

void SAThread::getHHistory(double *hist, int *length) {
  GET_HIST(h);
}

void SAThread::getVHistory(double *hist, int *length) {
  GET_HIST(v);
}

void SAThread::getQHistory(double *hist,int *length) {
  GET_HIST(q);
}

} // namespace LiberaBrilliance_ns
